// This file is a part of CMEDB software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to convert CME Group Inc. Daily bulletin reports from pdf to csv for further processing in spreadsheet applications.
// This version of CMEDB and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// ���� ���� �������� ������ ������� CMEDB ��������� ������� ���������� (tumypv@yandex.ru)
// ��� �������������� ���������� ������� ��������� ����� �� pdf � csv ��� ���������� ��������� � ����������� ��������
// ��� ������ CMEDB � � �������� ��� �������� ������������ ����������, ��� ������ ���
// �� ������ �������������� � �������� ��� � ����������� ��� ����� ����� ����� ��� ������� ��� ��� ����������� ������� ����������

// ���� ���� �������� �������� ���������� ������ ��� ������ ���������� iTextSharp

using iTextSharp.text.pdf.parser;
using System.Collections.Generic;
using System;
using System.Text;
namespace CMEDB {

    class CMEDB_ExtractionStrategy : ITextExtractionStrategy {
        private List<TextChunk> locationalResult = new List<TextChunk>();
        bool IsLocationalResultSorted = false;

        public CMEDB_ExtractionStrategy()
        {
        }

        public void BeginTextBlock(){
        }

        public void EndTextBlock(){
        }

        public string GetResultantText()
        {
            if (!IsLocationalResultSorted)
            {
                locationalResult.Sort();
                IsLocationalResultSorted = true;
            }

            StringBuilder sb = new StringBuilder();
            TextChunk lastChunk = null;

            var lineChunks = new List<TextChunk>();

            foreach (TextChunk chunk in locationalResult)
            {
                if (lastChunk != null)
                {
                    if (!chunk.SameLine(lastChunk))
                    {
                        sb.AppendLine(string.Join("", lineChunks.ConvertAll<string>(c => c.text).ToArray()));
                        lineChunks.Clear();
                    }
                }
                lineChunks.Add(chunk);
                lastChunk = chunk;
            }
            return sb.ToString();
        }

        public string GetFormattedText(DBPDFFormatInfo formatInfo)
        {
            if (!IsLocationalResultSorted)
            {
                locationalResult.Sort();
                IsLocationalResultSorted = true;
            }

            StringBuilder sb = new StringBuilder();
            TextChunk lastChunk = null;

            var lineChunks = new List<TextChunk>();

            double spacing = 0;
            int chunks = 0;
            int n = 0;

            foreach (TextChunk chunk in locationalResult)
            {
                if (n > 60 && n < 95)
                {
                    if (chunk.charSpaceWidth < 10)
                    {
                        chunks++;
                        spacing += chunk.charSpaceWidth;
                    }
                }

                if (lastChunk != null)
                {
                    if (!chunk.SameLine(lastChunk))
                    {
                        ProcessLine(lineChunks, sb, formatInfo.CharSpacing);
                        lineChunks.Clear();
                        sb.AppendLine();
                        n++;
                    }
                }

                lineChunks.Add(chunk);

                lastChunk = chunk;
            }

            return sb.ToString();
        }

        struct CP : IComparable<CP>
        {
            public char Char;
            public float Position;

            public int CompareTo(CP other)
            {
                return this.Position.CompareTo(other.Position);
            }
            public override string ToString()
            {
                return Char.ToString();
            }
        }

        private void ProcessLine(List<TextChunk> lineChunks, StringBuilder sb, float charSpacing)
        {
            var chars = new List<CP>();

            float cursor = 0;
            foreach (var chunk in lineChunks)
            {
                var chunkLength = chunk.distParallelEnd - chunk.distParallelStart;
                int spaceCount = 0, nonSpaceCount = 0;
                foreach (var ch in chunk.text)
                {
                    if (ch == ' ')
                        spaceCount += 1;
                    else
                        nonSpaceCount += 1;
                }
                float charWidth = (chunkLength - spaceCount * chunk.charSpaceWidth)/nonSpaceCount;

                cursor = chunk.distParallelStart;
                foreach (var ch in chunk.text)
                {
                    if (ch == ' ')
                    {
                        cursor += chunk.charSpaceWidth;
                    }
                    else
                    {
                        chars.Add(new CP() { Char = ch, Position = cursor });
                        cursor += charWidth;
                    }
                }
            }

            chars.Sort();
            var lineLenght = 0;
            foreach (var ch in chars)
            {
                var addSpaces = (int)(Math.Max(0, ch.Position / charSpacing - lineLenght));
                sb.Append(new string(' ', addSpaces));
                sb.Append(ch.Char);
                lineLenght += addSpaces + 1;
            }
        }

        public Dictionary<float, int> stats = new Dictionary<float, int>();
        
        public void RenderText(TextRenderInfo renderInfo) 
        {
            LineSegment segment = renderInfo.GetBaseline();
            var charSpaceWidth = renderInfo.GetSingleSpaceWidth();

            if (!stats.ContainsKey(charSpaceWidth))
                stats.Add(charSpaceWidth,0);
            stats[charSpaceWidth]++;

            if (!stats.ContainsKey(charSpaceWidth))
                stats.Add(charSpaceWidth,0);
            stats[charSpaceWidth]++;
            TextChunk location = new TextChunk(renderInfo.GetText(), segment.GetStartPoint(),
                segment.GetEndPoint(), charSpaceWidth);//, 3.2f
            locationalResult.Add(location);        
        }

        /// <summary>
        /// Represents a chunk of text, it's orientation, and location relative to the orientation vector
        /// </summary>
        private class TextChunk : IComparable<TextChunk>{
            /** the text of the chunk */
            internal String text;
            /** the starting location of the chunk */
            internal Vector startLocation;
            /** the ending location of the chunk */
            internal Vector endLocation;
            /** unit vector in the orientation of the chunk */
            internal Vector orientationVector;
            /** the orientation as a scalar for quick sorting */
            internal int orientationMagnitude;
            /** perpendicular distance to the orientation unit vector (i.e. the Y position in an unrotated coordinate system)
             * we round to the nearest integer to handle the fuzziness of comparing floats */
            internal float distPerpendicular;
            /** distance of the start of the chunk parallel to the orientation unit vector (i.e. the X position in an unrotated coordinate system) */
            internal float distParallelStart;
            /** distance of the end of the chunk parallel to the orientation unit vector (i.e. the X position in an unrotated coordinate system) */
            internal float distParallelEnd;
            /** the width of a single space character in the font of the chunk */
            internal float charSpaceWidth;

            //internal float fontCharSpacing;

            public TextChunk(String str, Vector startLocation, Vector endLocation, float charSpaceWidth)
            {//, float fontCharSpacing
                this.text = str;
                this.startLocation = startLocation;
                this.endLocation = endLocation;
                this.charSpaceWidth = charSpaceWidth;
                //this.fontCharSpacing = fontCharSpacing;
                
                Vector oVector = endLocation.Subtract(startLocation);
                if (oVector.Length == 0) {
                    oVector = new Vector(1, 0, 0);
                }
                orientationVector = oVector.Normalize();
                orientationMagnitude = (int)(Math.Atan2(orientationVector[Vector.I2], orientationVector[Vector.I1])*1000);

                // see http://mathworld.wolfram.com/Point-LineDistance2-Dimensional.html
                // the two vectors we are crossing are in the same plane, so the result will be purely
                // in the z-axis (out of plane) direction, so we just take the I3 component of the result
                Vector origin = new Vector(0,0,1);
                distPerpendicular = (startLocation.Subtract(origin)).Cross(orientationVector)[Vector.I3];

                distParallelStart = orientationVector.Dot(startLocation);
                distParallelEnd = orientationVector.Dot(endLocation);
            }

            public void PrintDiagnostics(){
                Console.WriteLine("Text (@" + startLocation + " -> " + endLocation + "): " + text);
                Console.WriteLine("orientationMagnitude: " + orientationMagnitude);
                Console.WriteLine("distPerpendicular: " + distPerpendicular);
                Console.WriteLine("distParallel: " + distParallelStart);
            }
            
            /**
             * @param as the location to compare to
             * @return true is this location is on the the same line as the other
             */
            public bool SameLine(TextChunk a){
                if (orientationMagnitude != a.orientationMagnitude) return false;
                if (CompareFloats(distPerpendicular, a.distPerpendicular, 2.5f) != 0) return false;
                //if (distPerpendicular -2 > a.distPerpendicular) return false;
                return true;
            }

            /**
             * Computes the distance between the end of 'other' and the beginning of this chunk
             * in the direction of this chunk's orientation vector.  Note that it's a bad idea
             * to call this for chunks that aren't on the same line and orientation, but we don't
             * explicitly check for that condition for performance reasons.
             * @param other
             * @return the number of spaces between the end of 'other' and the beginning of this chunk
             */
            public float DistanceFromEndOf(TextChunk other){
                float distance = distParallelStart - other.distParallelEnd;
                return distance;
            }
            
            /**
             * Compares based on orientation, perpendicular distance, then parallel distance
             * @see java.lang.Comparable#compareTo(java.lang.Object)
             */
            public int CompareTo(TextChunk rhs) {
                if (this == rhs) return 0; // not really needed, but just in case
                
                int rslt;
                rslt = CompareInts(orientationMagnitude, rhs.orientationMagnitude);
                if (rslt != 0) return rslt;

                rslt = CompareFloats(distPerpendicular, rhs.distPerpendicular, 2.5f);
                if (rslt != 0) return rslt;

                // note: it's never safe to check floating point numbers for equality, and if two chunks
                // are truly right on top of each other, which one comes first or second just doesn't matter
                // so we arbitrarily choose this way.
                rslt = distParallelStart < rhs.distParallelStart ? -1 : 1;

                return rslt;
            }

            /**
             *
             * @param int1
             * @param int2
             * @return comparison of the two integers
             */
            private static int CompareInts(int int1, int int2){
                return int1 == int2 ? 0 : int1 < int2 ? -1 : 1;
            }

            static int CompareFloats(float f1, float f2, float threshold)
            {
                if (Math.Abs(f1 - f2) < threshold)
                    return 0;
                return f1.CompareTo(f2);
            }

            public override string ToString()
            {
                return text;
            }
        }

        public void RenderImage(ImageRenderInfo renderInfo) { }
    }
}