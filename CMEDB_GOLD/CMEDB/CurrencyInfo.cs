﻿// This file is a part of CMEDB software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to convert CME Group Inc. Daily bulletin reports from pdf to csv for further processing in spreadsheet applications.
// This version of CMEDB and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты CMEDB созданной Тимуром Вишняковым (tumypv@yandex.ru)
// Для преобразования ежедневных отчётов Чикагской биржи из pdf в csv для дальнейшей обработки в электронных таблицах
// Эта версия CMEDB и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

// Этот файл содержит информацию о поддерживаемых секциях и инструментах

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml;

namespace CMEDB
{
    class CurrencyInfo
    {
        public static List<CurrencyInfo> AvailableCurrencyInfoList {get; private set;}

        [Category("1. Общие"), DisplayName("Forex название"), Description("Название этого торгового инструмента на форекс")]
        [System.ComponentModel.ParenthesizePropertyName]
        public string ForexName { get; private set; }
        [Category("1. Общие"), DisplayName("Тикер фьючерса"), Description("Постоянная часть тикера фьючерса для этого торгового инструмента")]
        public string FuturesTicker { get; private set; }
        [Category("1. Общие"), DisplayName("Обратная пара"), Description("Если True (истина), то это обратная пара (как USDCAD), иначе False (как GBPUSD).")]
        public bool IsReverse { get; private set; }
        [Category("1. Общие"), DisplayName("Точность фьючерса"), Description("Кол-во знаков после запятой в котировке фьючерса")]
        public int FuturesPrecision { get; private set; }
        [Category("4. PDF"), DisplayName("Контракт CALL"), Description("Название контракта CALL в DailyBulletin")]
        public string CallContractName { get; private set; }
        [Category("4. PDF"), DisplayName("Контракт PUT"), Description("Название контракта PUT в DailyBulletin")]
        public string PutContractName { get; private set; }
        [Category("4. PDF"), DisplayName("Секция CALL"), Description("Код секции, содержащей контракты CALL в DailyBulletin")]
        public int CallSection { get; private set; }
        [Category("4. PDF"), DisplayName("Секция PUT"), Description("Код секции, содержащей контракты PUT в DailyBulletin")]
        public int PutSection { get; private set; }
        [Category("4. PDF"), DisplayName("Секция фьючерсов"), Description("Код секции, содержащей фьючерсы в DailyBulletin")]
        public int FutSection { get; private set; }
        [Category("4. PDF"), DisplayName("Фьючерс"), Description("Название раздела с фьючерсами в DailyBulletin")]
        public string FuturesName { get; private set; }
        [Category("4. PDF"), DisplayName("Множитель страйка"), Description("Значение, на которое нужно умножить страйк, чтобы получить его цену в пипках.")]
        public decimal StrikeMultiplier { get; private set; }
        [Category("4. PDF"), DisplayName("Шаг страйков"), Description("Значение, которое нужно прибавить к страйку, чтобы получить следующий страйк.")]
        public int StrikeStep { get; private set; }
        [Category("4. PDF"), DisplayName("Множитель премии"), Description("Значение, на которое нужно умножить премию, чтобы получить её цену в пипках.")]
        public decimal SettPriceMultiplier { get; private set; }
        [Category("4. PDF"), DisplayName("Множитель forward points"), Description("Значение, на которое нужно умножить froward points, чтобы перевести его в кол-во пипок (4 знака).")]
        public decimal ForwardPointsMultiplier { get; private set; }

        public string FormatFuturesPrice(decimal price)
        {
            return price.ToString("0." + new string('0', FuturesPrecision), System.Globalization.CultureInfo.InvariantCulture);
        }

        public CurrencyInfo(string forexName, string FuturesTicker, string callContractName, string putContractName, string futuresName, int callSection, int putSection, int futSection,
            decimal strikeMultiplier, int strikeStep, decimal settPriceMultiplier, decimal spotMultiplier, bool isReverse, int FuturesPrecision)
        {
            this.ForexName = forexName;
            this.FuturesTicker = FuturesTicker;
            this.CallSection = callSection;
            this.PutSection = putSection;
            this.FutSection = futSection;
            this.CallContractName = callContractName;
            this.PutContractName = putContractName;
            this.FuturesName = futuresName;

            this.StrikeMultiplier = strikeMultiplier;
            this.StrikeStep = strikeStep;
            this.SettPriceMultiplier = settPriceMultiplier;
            this.ForwardPointsMultiplier = spotMultiplier;
            this.IsReverse = isReverse;
            this.FuturesPrecision = FuturesPrecision;
        }

        static CurrencyInfo()
        {
            AvailableCurrencyInfoList = new List<CurrencyInfo>();
            AvailableCurrencyInfoList.Add(new CurrencyInfo("GBP", "6B", "BRIT PND CALL", "BRIT PND PUT", "BRIT PND FUT", 27, 28, 27, 0.001m, 5, 0.01m, 0.0001m, false, 4));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("EUR", "6E", "EURO FX CALL", "EURO FX PUT", "EURO FX FUT", 39, 39, 39, 0.001m, 5, 0.001m, 0.0001m, false, 4));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("CAD", "6C", "CANADA DLR CALL", "CANADA DLR PUT", "CANADA DLR FUT", 29, 30, 39, 0.001m, 5, 0.01m, 0.0001m, true, 4));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("AUD", "6A", "AUST DLR CALL", "AUST DLR PUT", "AUST DLR FUT", 38, 38, 38, 0.001m, 5, 0.01m, 0.0001m, false, 4));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("JPY", "6J", "JAPAN YEN CALL", "JAPAN YEN PUT", "JAPAN YEN FUT", 33, 34, 33, 0.00001m, 5, 0.0001m, 0.01m, true, 6));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("CHF", "6S", "SWISS FRNC CALL", "SWISS FRNC PUT", "SWISS FRNC FUT", 35, 36, 35, 0.001m, 5, 0.01m, 0.0001m, true, 4));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("GOLD", "GC", "OG CALL", "OG PUT", "COMEX GOLD FUTURES", 64, 64, 62, 1m, 5, 1m, 1m, false, 2));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("SILVER", "SI", "SO CALL", "SO PUT", "COMEX SILVER FUTURES", 64, 64, 62, 0.01m, 25, 1m, 0.01m, false, 3));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("Corn", "ZC", "CORN CALL", "CORN PUT", "CORN FUTURES", 56, 56, 56, 1m, 5, 0.1m, 1m, false, 2));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("SOYBEAN", "ZS", "SOYBEAN CALL", "SOYBEAN PUT", "CORN FUTURES", 57, 57, 57, 1m, 10, 0.1m, 1m, false, 2));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("WHEAT", "ZW", "WHEAT CALL", "WHEAT PUT", "WHEAT FUT", 57, 57, 57, 1m, 5, 0.1m, 1m, false, 2));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("Crude Oil", "CL", "LO CALL", "LO PUT", "LO FUT", 63, 63, 63 /* 61, но пока не поддерживается */, 0.01m, 50, 1m, 1m, false, 2));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("Natural Gas", "NG", "ON CALL", "ON PUT", "ON FUT", 63, 63, 63, 0.001m, 50, 1m, 1m, false, 3));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("E-MINI S&P", "ES", "EMINI S&P CALL", "EMINI S&P PUT", "EMINI S&P FUT", 47, 48, 47, 1m, 5, 1m, 1m, false, 2));
        }



    }
}
