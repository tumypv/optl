﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using CMEDB;
using System.Xml;

namespace OptL
{
    public class DBParser : ICMEDBParserListener
    {
        public string Path { get; set; }
        public string FileName { get; set; }
        public DateTime Date { get; set; }
        public string BulletinType { get; set; }

        //public FuturesInfo FirstFutures;
        //public FuturesInfo NextFutures;

        public List<SectionData> sections = new List<SectionData>();

        //Dictionary<string, object> processedContracts = new Dictionary<string, object>();

        SectionData currentSection = null;
        ContractData currentContract = new ContractData();

        int c_total_oi = 0;
        int StrikeCount = 0;

        public DBParser()
        {
            MinOi = new CurrencySpecificProperty<int>();
            MaxOi = new CurrencySpecificProperty<int>();
            MinOiCh = new CurrencySpecificProperty<int>();
            MaxOiCh = new CurrencySpecificProperty<int>();
            MinVol = new CurrencySpecificProperty<int>();
            MaxVol = new CurrencySpecificProperty<int>();

            ForwardPoints = new CurrencySpecificProperty<int>();
            //SpotAdjustmentCall = new CurrencySpecificProperty<int>();
            //SpotAdjustmentPut = new CurrencySpecificProperty<int>();
        }

        public void Error(Exception ex, string line)
        {
            Console.WriteLine(line);
        }

        public void ProcessSectionInfo(int bulletinSectionCode, string bulletinName, int bulletinNumber, DateTime bulletinDate, bool isFinal)
        {
            if (bulletinSectionCode == 62 || bulletinSectionCode == 64)
                currentSection = sections.Find(s => s.Code == 62 || s.Code == 64);
            else
                currentSection = null;
            
            if (currentSection == null)
            {
                currentSection = new SectionData();
                currentSection.Code = bulletinSectionCode == 62 ? 64 : bulletinSectionCode;
                sections.Add(currentSection);
            }

            this.Date = bulletinDate;
            this.BulletinType = isFinal ? "FINAL" : "PRELIMINARY";
        }

        public void ProcessStop(string line)
        {

        }

        public void ProcessStrike(CMEDB.Strike strike)
        {
            //if (processedContracts.ContainsKey(strike.Name))
            //    return;
            StrikeCount++;
            c_total_oi += strike.OpenInterest;

            currentContract.Strikes.Add(strike);
        }

        internal void Save(XmlWriter xw)
        {
            xw.WriteStartElement("DB");
            xw.WriteAttributeString("Path", Path);
            xw.WriteAttributeString("FileName", FileName);
            xw.WriteAttributeString("Date", XmlConvert.ToString(Date, XmlDateTimeSerializationMode.Unspecified));
            xw.WriteAttributeString("BulletinType", BulletinType);

            foreach (var section in sections)
            {
                xw.WriteStartElement("Section");
                xw.WriteAttributeString("Code", section.Code.ToString());

                xw.WriteStartElement("FuturesList");
                foreach (var futures in section._FuturesList)
                    futures.WriteXML(xw);
                xw.WriteEndElement();

                xw.WriteStartElement("ContractList");

                foreach (var c in section.Contracts)
                {
                    xw.WriteStartElement("Contract");
                    xw.WriteAttributeString("Name", c.Name);
                    xw.WriteAttributeString("Date", c.Date.ToBinary().ToString());
                    
                    foreach (var s in c.Strikes)
                        s.WriteXML(xw);

                    xw.WriteEndElement();
                }
                xw.WriteEndElement();

                xw.WriteEndElement();
            }

            xw.WriteEndElement();
        }

        public static DBParser Load(XmlReader xr)
        {
            xr.Read();
            DBParser l = new DBParser();
            l.Path = xr.GetAttribute("Path");
            l.FileName = xr.GetAttribute("FileName");
            l.Date = XmlConvert.ToDateTime(xr.GetAttribute("Date"), XmlDateTimeSerializationMode.Unspecified);
            l.BulletinType = xr.GetAttribute("BulletinType");

            while (xr.Read() && xr.NodeType != XmlNodeType.EndElement)
            {
                l.currentSection = new SectionData() { Code = int.Parse(xr.GetAttribute("Code")) };

                xr.Read(); // <FuturesList>
                if (!xr.IsEmptyElement)
                {
                    ReadFuturesList(xr, l);
                }
                else
                {
                    xr.Read();
                }

                ReadContractList(xr, l);
                l.sections.Add(l.currentSection);
            }
            //xr.ReadEndElement(); // </DB>
            return l;
        }

        private static void ReadContractList(XmlReader xr, DBParser l)
        {
            while (xr.Read() && xr.NodeType != XmlNodeType.EndElement)
            {
                var contract = new ContractData()
                {
                    Date = DateTime.FromBinary(long.Parse(xr.GetAttribute("Date"))),
                    Name = xr.GetAttribute("Name")
                };

                while (xr.Read() && xr.NodeType != XmlNodeType.EndElement)
                {
                    var s = Strike.ReadXML(xr, contract.Name, contract.Date);
                    contract.Strikes.Add(s);
                }

                l.currentSection.Contracts.Add(contract);
            }

            xr.ReadEndElement(); //</ContractList>
        }

        private static void ReadFuturesList(XmlReader xr, DBParser l)
        {
            while (xr.Read() && xr.NodeType != XmlNodeType.EndElement)
            {
                l.currentSection._FuturesList.Add(FuturesInfo.ReadXML(xr));
            }
            xr.ReadEndElement(); // </FuturesList>
        }

        public SectionData GetSection(int sectionCode)
        {
            return sections.Find(s => s.Code == sectionCode);
        }

        void ICMEDBParserListener.ProcessTotal(int total_vol, int total_oi, int total_oi_ch, string contractName, DateTime contractDate, decimal futuresSettPrice)
        {
            if
            (
                c_total_oi > 0 // Если OI контракта = 0, пропускаем его
                && !contractName.StartsWith("WK") // недельный опцион, пропускаем
                //TODO: Нужно что-то лучше придумать, чтобы не загружать только нужные опционы
            )
            {
                currentContract.Name = contractName;
                currentContract.Date = contractDate;
                currentContract.FuturesSettPrice = futuresSettPrice;
                //processedContracts.Add(contractName, null);
                currentSection.Contracts.Add(currentContract);
            }

            c_total_oi = 0;
            StrikeCount = 0;
            currentContract = new ContractData();
        }

        public void ProcessFut(FuturesInfo fut)
        {
            //TODO: Нужно что-то лучше придумать, чтобы не загружать лишних фьючерсов
            if (fut.Name != "EC/BP CROSS RT" && fut.Name != "NEW ZEALND FUT")
            {
                if (fut.OpenInterest > 0) // пропускаем экспирационный фьючерс
                    currentSection._FuturesList.Add(fut);
                else
                {
                    ;
                }
            }
        }

        public void ProcessFutTotal(int rth_vol, int globex_vol, int oi, int oi_ch)
        {
        }

        public override string ToString()
        {
            return this.FileName;
        }

        public CurrencySpecificProperty<int> MaxOi { get; private set; }

        public CurrencySpecificProperty<int> MinOi { get; private set; }

        public CurrencySpecificProperty<int> ForwardPoints { get; private set; }

        public CurrencySpecificProperty<int> MaxVol { get; private set; }

        public CurrencySpecificProperty<int> MinVol { get; private set; }

        public CurrencySpecificProperty<int> MaxOiCh { get; private set; }

        public CurrencySpecificProperty<int> MinOiCh { get; private set; }


        public DateTime LevelDate
        {
            get
            {
                switch (Date.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                    case DayOfWeek.Tuesday:
                    case DayOfWeek.Wednesday:
                    case DayOfWeek.Thursday:
                        return Date.AddDays(1);
                    case DayOfWeek.Friday:
                        return Date.AddDays(3);
                    default:
                        throw new Exception();
                }
            }
        }

        public void GetFirstContract(CurrencyInfo ci, bool skipExpired, out ContractData call, out ContractData put)
        {
            GetContractN(ci, 1, out call, out put);
        }

        public void GetContractN(CurrencyInfo ci, int N, out ContractData call, out ContractData put)
        {
            var calls = GetSection(ci.CallSection).Contracts.FindAll(c => c.Name.StartsWith(ci.CallContractName));
            var puts = GetSection(ci.PutSection).Contracts.FindAll(c => c.Name.StartsWith(ci.PutContractName));
            
            
            int cn = 1;

            foreach (var callContract in calls)
            {
                var putContract = puts.Find(pc => pc.Date == callContract.Date);

                var c = callContract.Strikes.Find(s => s.OpenInterest > 0);
                var p = putContract.Strikes.Find(s => s.OpenInterest > 0);

                if (c == null || p == null)
                {
                    if (cn == 1)
                        continue;
                    else
                        throw new Exception();
                }

                if (cn == N)
                {
                    call = callContract;
                    put = putContract;
                    return;
                }
                cn++;
            }
            throw new Exception("Действующий опционный контракт не обнаружен");
        }

        public string FillMask(string fileOut)
        {
            throw new NotImplementedException();
        }
    }

    public class CurrencySpecificProperty<T> where T : struct
    {
        Dictionary<string, T> d = new Dictionary<string, T>();

        public T? this[string index]
        {
            get
            {
                if (!d.ContainsKey(index))
                    return null;
                return d[index];
            }
            set
            {
                if (!value.HasValue)
                {
                    if (d.ContainsKey(index))
                        d.Remove(index);
                }
                else
                    d[index] = value.Value;
            }
        }
    }

    public class SectionData
    {
        public int Code;
        public List<FuturesInfo> _FuturesList = new List<FuturesInfo>();
        public List<ContractData> Contracts = new List<ContractData>();

        public FuturesInfo GetFuturesByOption(DateTime optionDate)
        {
            //MAR13 <- FEB13,MAR13;APR13
            //JUN13 <- MAY13;JUN13;JUL13
            //SEP13 <- AUG13;SEP13;OCT13
            //DEC13 <- NOV13;DEC13;JAN14

            var futuresYear = optionDate.Year;
            var futuresMonth = 0;

            switch (optionDate.Month)
            {
                case 2: case 3: case 4: futuresMonth = 3; break;
                case 5: case 6: case 7: futuresMonth = 6; break;
                case 8: case 9: case 10: futuresMonth = 9; break;
                case 11: case 12: futuresMonth = 12; break;
                case 1: futuresMonth = 12; futuresYear = optionDate.Year - 1; break;
                default:
                    throw new Exception();
            }

            FuturesInfo res = null;            
            foreach (var f in _FuturesList)
            {
                if (f.Date.Month == futuresMonth && f.Date.Year == futuresYear)
                {
                    res = f;
                    break;
                }
            }

            if (res == null)
                res = _FuturesList[0];

            return res;
        }
    }

    public class ContractData
    {
        public string Name;
        public DateTime Date;
        public decimal FuturesSettPrice;
        public List<Strike> Strikes = new List<Strike>();
    }
}
