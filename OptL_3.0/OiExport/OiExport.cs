﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using MT4Tools;
using System.ComponentModel;
using System.Xml;
using CMEDB;

namespace OptL
{
    //[LevelsGeneratorSettingsEditor("OptL.OiExportGeneratorSettingsEditor")]
    public class OiExportGenerator : LevelsGenerator
    {
        public override string ToString()
        {
            return "Выгрузка открытого интереса и объёмов";
        }

        List<CMEDB.Strike> GetCalls(DBParser db, CurrencyInfo ci)
        {
            var callSection = db.GetSection(ci.CallSection);

            return db.GetSection(ci.CallSection)
                .Contracts.Find(c => c.Name == ci.CallContractName)
                .Strikes;
        }

        List<CMEDB.Strike> GetPuts(DBParser db, CurrencyInfo ci)
        {
            var putSection = db.GetSection(ci.PutSection);
            return db.GetSection(ci.PutSection)
                .Contracts.Find(c => c.Name == ci.PutContractName)
                .Strikes;
        }

        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            string format = "{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}";

            StringBuilder res = new StringBuilder();


            res.AppendLine(string.Format(format,
                "db.Date", "db.LevelDate",
                "call_oi", "call_oich", "call_vol", "put_oi", "put_oich", "put_vol",
                "futuresOI", "futuresOICH", "futuresVol"));

            foreach (var db in dbs)
            {
                int call_oi = 0, put_oi = 0, call_oich = 0, put_oich = 0, call_vol = 0, put_vol = 0;

                foreach (var call in GetCalls(db, ci))
                {
                    call_oi += call.OpenInterest;
                    call_oich += call.OpenInterestChange;
                    call_vol += call.VolumeTradesCleared;
                }

                foreach (var put in GetPuts(db, ci))
                {
                    put_oi += put.OpenInterest;
                    put_oich += put.OpenInterestChange;
                    put_vol += put.VolumeTradesCleared;
                }

                string futuresOI = "", futuresOICH = "", futuresVol = "";
                var fut = db.GetSection(ci.CallSection)._FuturesList;
                if (fut.Count > 0)
                {
                    futuresOI = fut[0].OpenInterest.ToString();
                    futuresOICH = fut[0].OpenInterestChange.ToString();
                    futuresVol = fut[0].GlobexVolume.ToString();
                }

                res.AppendLine
                (
                    string.Format(format,
                        db.Date.ToShortDateString(), db.LevelDate.ToShortDateString(),
                        call_oi, call_oich, call_vol, put_oi, put_oich, put_vol,
                        futuresOI, futuresOICH, futuresVol)
                );
            }

            var fs = new System.Windows.Forms.SaveFileDialog();
            fs.CheckPathExists = true;
            fs.FileName = "oi.csv";
            fs.Filter = "Excel файл CSV|csv";
            fs.Title = ToString();
            if (fs.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.File.WriteAllText(fs.FileName, res.ToString());
            }
        }

        public override void GenerateText(StringBuilder sb, CurrencyInfo ci, bool spot, DBParser db)
        {
        }

        public override void SaveSettings(XmlElement settings)
        {
        }

        public override void LoadSettings(XmlElement genSettings)
        {
        }
    }
}
