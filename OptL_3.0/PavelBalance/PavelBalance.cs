﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using OptL;
using System.ComponentModel;
using System.Xml;

namespace PavelBalance
{
    public class PavelBalance : LevelsGenerator
    {
        public override string ToString()
        {
            return "Уровень Павла";
        }

        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            foreach (var db in dbs)
            {
                GenerateDayLevels(templateBuilder, ci, db);
            }
        }

        class PLevel : OptLevel
        {
            public PLevel(CMEDB.Strike s, CurrencyInfo ci, bool isCall) : base(s, ci, isCall) { }
            public decimal Weight2 { get; set; }
        }

        private void GenerateDayLevels(ITemplateBuilder templateBuilder, CurrencyInfo ci, DBParser db)
        {
            //Ищется уровень сверху по путам где объем помноженный на премию макс и такой же по коллам а потом среднее между ними

            var callSection = db.GetSection(ci.CallSection);
            var putSection = db.GetSection(ci.PutSection);

            PLevel call = null;
            PLevel call2 = null;

            db.GetSection(ci.CallSection)
                .Contracts.Find(c => c.Name.StartsWith(ci.CallContractName))
                .Strikes.ConvertAll(s => new PLevel(s, ci, true) { Weight = s.VolumeTradesCleared * s.SettPrice, Weight2 = s.OpenInterest * s.SettPrice } )
                .ForEach(ol => {
                    call = (call == null || ol.Weight > call.Weight) ? ol : call;
                    call2 = (call2 == null || ol.Weight2 > call2.Weight2) ? ol : call2;
                });

            PLevel put = null;
            PLevel put2 = null;
            db.GetSection(ci.PutSection)
                .Contracts.Find(c => c.Name.StartsWith(ci.PutContractName))
                .Strikes.ConvertAll(s => new PLevel(s, ci, false) { Weight = s.VolumeTradesCleared * s.SettPrice, Weight2 = s.OpenInterest * s.SettPrice })
                .ForEach(ol =>
                {
                    put = (put == null || ol.Weight > put.Weight) ? ol : put;
                    put2 = (put2 == null || ol.Weight2 > put2.Weight2) ? ol : put2;
                });

            DateTime levelDate;

            switch (db.Date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                    levelDate = db.Date.AddDays(1); break;
                case DayOfWeek.Friday:
                    levelDate = db.Date.AddDays(3);
                    break;
                default:
                    throw new Exception();
            }

            var description =
                call.Strike.OpenInterest
                + (call.Strike.OpenInterestChange >= 0 ? "+" : "")
                + call.Strike.OpenInterestChange
                + " V=" + call.Strike.VolumeTradesCleared
                + " Pt=" + call.Strike.SettPrice;

            templateBuilder.AddLevel(call.Level, call.Level, levelDate, db, 1f, call.Strike.StrikeValue.ToString(), description, "K2_TOP1");
            if (WeightingMethod == WeightingMethod.OI)
                templateBuilder.AddLevel(call2.Level, call2.Level, levelDate, db, 1f, call2.Strike.StrikeValue.ToString(), "OI call", "K3_SELL2");

            description =
                put.Strike.OpenInterest
                + (put.Strike.OpenInterestChange >= 0 ? "+" : "")
                + put.Strike.OpenInterestChange
                + " V=" + put.Strike.VolumeTradesCleared
                + " Pt=" + put.Strike.SettPrice;

            templateBuilder.AddLevel(put.Level, put.Level, levelDate, db, 1, put.Strike.StrikeValue.ToString(), description, "K2_BOTTOM1");
            if (WeightingMethod == WeightingMethod.OI)
                templateBuilder.AddLevel(put2.Level, put2.Level, levelDate, db, 1f, put2.Strike.StrikeValue.ToString(), "OI put", "K3_BUY2");

            var balanceY = (call.Level + put.Level) / 2;
            templateBuilder.AddLevel(balanceY, balanceY, levelDate, db, 1, call.Strike.StrikeValue.ToString(), "", "K2_BALANCEY");

            if (WeightingMethod == WeightingMethod.OI)
            {
                var balanceW = (call2.Level + put2.Level) / 2;
                templateBuilder.AddLevel(balanceW, balanceW, levelDate, db, 1, call.Strike.StrikeValue.ToString(), "OI middle", "PR_MONDAY_TARGET");
            }

        }

        private WeightingMethod weightingMethod = WeightingMethod.Volume;
        [DisplayName("Метод расчёта")]
        [Description("")]
        [DefaultValue(WeightingMethod.Volume)]
        public WeightingMethod WeightingMethod
        {
            get
            {
                return weightingMethod;
            }
            set
            {
                if (weightingMethod != value)
                {
                    weightingMethod = value;
                    OnPropertyChanged("WeightingMethod");
                }
            }
        }

        public override void SaveSettings(XmlElement settings)
        {
            settings.SetAttribute("WeightingMethod", this.WeightingMethod.ToString());
        }

        public override void LoadSettings(XmlElement genSettings)
        {
            if (genSettings.HasAttribute("WeightingMethod"))
                WeightingMethod = (WeightingMethod)Enum.Parse(typeof(WeightingMethod), genSettings.GetAttribute("WeightingMethod"));
        }
    }

    public enum WeightingMethod
    {
        [FieldDisplayName("объём (классический)")]
        Volume,
        [FieldDisplayName("объём и ОИ")]
        OI,
    }
}
