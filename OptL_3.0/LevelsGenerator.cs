﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using MT4Tools;
using CMEDB;
using System.Xml;

namespace OptL
{
    public abstract class LevelsGenerator : INotifyPropertyChanged
    {
        //[Browsable(false)]
        //public virtual bool UsesCashFlowHorizontal { get { return false; } }

        public abstract void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs);

        public virtual void GenerateText(StringBuilder sb, CurrencyInfo ci, bool spot, DBParser selectedDb)
        {
        }

        /// <summary>
        /// Будет вызвано до GenerateTemplate в потоке интерфейса, а потому здесь следует выводить дополнительные окна, но не выполнять тяжёлые операции
        /// </summary>
        /// <param name="ci"></param>
        /// <param name="spot"></param>
        /// <param name="selectedDb"></param>
        public virtual void ShowUI(CurrencyInfo ci, bool spot, IEnumerable<DBParser> dbs)
        {
        }

        //[Browsable(false)]
        //public Dictionary<DateTime, List<LevelVolumes>> CashFlowHorizontal { get; set; }

        [Browsable(false)]
        public virtual bool WritesTemplate { get { return true; } }

        //private CurrencyInfo currency;
        //private DBParser dbParser;

        //[Browsable(false)]
        //public virtual CurrencyInfo Currency
        //{
        //    get;
        //    set;
        //    //get { return currency; }
        //    //set
        //    //{
        //    //    if (this.currency != value)
        //    //    {
        //    //        this.currency = value;
        //    //        OnPropertyChanged("Currency");
        //    //    }
        //    //}
        //}

        //[Browsable(false)]
        //public virtual DBParser DbParser
        //{
        //    get;
        //    set;
        //    //get { return dbParser; }
        //    //set
        //    //{
        //    //    if (this.dbParser != value)
        //    //    {
        //    //        this.dbParser = value;
        //    //        OnPropertyChanged("DbParser");
        //    //    }
        //    //}
        //}

        public event PropertyChangedEventHandler PropertyChanged;

        public abstract void SaveSettings(XmlElement settings);

        protected void OnPropertyChanged(string propertyName)
        {
            var h = PropertyChanged;
            if (h != null)
                h(this, new PropertyChangedEventArgs(propertyName));
        }

        public abstract void LoadSettings(XmlElement genSettings);

        [Browsable(false)]
        public CurrencyInfo[] CurrencyList { get; set; }
    }

    public class LevelsGeneratorAdditionalFile
    {
        public string Name;
        public byte[] Data;
    }

    public abstract class LevelsGenerator2 : LevelsGenerator
    {
        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            throw new NotImplementedException();
        }

        public abstract IEnumerable<LevelsGeneratorAdditionalFile> GenerateTemplate(IEnumerable<DBParser> dbs, CurrencyInfo ci, ITemplateBuilder templateBuilder);
        //{
        //    GenerateTemplate(dbs, ci, templateBuilder);
        //    return null;
        //}
    }

    interface ILevelsGeneratorSettingsEditor
    {
        INotifyPropertyChanged Settings { get; set; }
    }

    public class OptLevel
    {
        public OptLevel(Strike s, CurrencyInfo ci, bool isCall)
        {
            this.Strike = s;
            this.StrikeLevel = Strike.StrikeValue * ci.StrikeMultiplier;
            this.Level = this.StrikeLevel + (isCall ? 1 : -1) * Strike.SettPrice * ci.SettPriceMultiplier;
        }

        public decimal Weight { get; set; }
        public Strike Strike { get; private set; }
        public decimal Level { get; private set; }
        public decimal StrikeLevel { get; private set; }
    }
}


public class LevelGeneratorException : Exception
{
    public LevelGeneratorException(string message)
        : base(message)
    {
    }
}