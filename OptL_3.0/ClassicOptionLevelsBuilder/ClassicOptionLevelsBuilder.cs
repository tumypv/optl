﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using MT4Tools;
using System.ComponentModel;
using System.Xml;

namespace OptL
{
    //[LevelsGeneratorSettingsEditor("OptL.ClassicOptionLevelsGeneratorSettingsEditor")]
    public class ClassicOptionLevelsGenerator : LevelsGenerator
    {
        public override string ToString()
        {
            return "Классические опционные уровни";
        }

        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            foreach (var db in dbs)
                ProcessDB(templateBuilder, ci, db);
        }

        private ContractData GetContract(int section, string contractName, DBParser db)
        {
            var sectionData = db.GetSection(section);

            int n = 0;
            foreach (var contract in sectionData.Contracts)
            {
                if (!contract.Name.StartsWith(contractName))
                    continue;
                n++;

                if ((forContractNumber.HasValue && n != forContractNumber) || (forContractDate.HasValue && contract.Date != forContractDate))
                    continue;

                return contract;
            }

            return null;
            //return (forContractNumber.HasValue && c.N != forContractNumber) || (forContractDate.HasValue && c.ExpirationDate != forContractDate);
        }

        class StrikeFilter
        {
            int? minOi;
            int? maxOi;
            int? minOiCh;
            int? maxOiCh;
            int? minVol;
            int? maxVol;
            bool hideCab;

            public StrikeFilter(ClassicOptionLevelsGenerator g, DBParser db, CurrencyInfo ci)
            {
                minOi = g.IgnoreOiThreshold ? null : db.MinOi[ci.ForexName];
                maxOi = g.IgnoreOiThreshold ? null : db.MaxOi[ci.ForexName];
                minOiCh = g.IgnoreOiChThreshold ? null : db.MinOiCh[ci.ForexName];
                maxOiCh = g.IgnoreOiChThreshold ? null : db.MaxOiCh[ci.ForexName];
                minVol = g.IgnoreVolThreshold ? null : db.MinVol[ci.ForexName];
                maxVol = g.IgnoreVolThreshold ? null : db.MaxVol[ci.ForexName];
                this.hideCab = g.HideCab;
            }

            public bool Filter(CMEDB.Strike s)
            {
                return
                    (!minOi.HasValue || s.OpenInterest >= minOi)
                    && (!maxOi.HasValue || s.OpenInterest <= maxOi)

                    && (!minOiCh.HasValue || s.OpenInterestChange >= minOiCh)
                    && (!maxOiCh.HasValue || s.OpenInterestChange <= maxOiCh)

                    && (!minVol.HasValue || s.VolumeTradesCleared >= minVol)
                    && (!maxVol.HasValue || s.VolumeTradesCleared <= maxVol)

                    && (!hideCab || !s.SettPriceCab);
            }
        }

        public override void GenerateText(StringBuilder sb, CurrencyInfo ci, bool spot, DBParser db)
        {
            var tf = new StrikeFilter(this, db, ci);

            sb.AppendLine(this.ToString());
            var calls = GetContract(ci.CallSection, ci.CallContractName, db)
                .Strikes.FindAll(tf.Filter);

            var puts = GetContract(ci.PutSection, ci.PutContractName, db)
                .Strikes.FindAll(tf.Filter);

            calls.Sort(TextLevelsSortComparison);
            puts.Sort(TextLevelsSortComparison);

            sb.AppendLine(ci.CallContractName);
            sb.AppendLine("OI\tOI_CH\tLevel\tVolumes");
            foreach (var call in calls)
            {
                var ol = new OptLevel(call, ci, true);
                sb.AppendLine(ol.Strike.OpenInterest + "\t" + ol.Strike.OpenInterestChange + "\t" + ol.Level.ToString("0.0000") + "\t" + ol.Strike.VolumeTradesCleared);
            }
            sb.AppendLine(ci.PutContractName);
            foreach (var put in puts)
            {
                var ol = new OptLevel(put, ci, false);
                sb.AppendLine(ol.Strike.OpenInterest + "\t" + ol.Strike.OpenInterestChange + "\t" + ol.Level.ToString("0.0000") + "\t" + ol.Strike.VolumeTradesCleared);
            }
            sb.AppendLine();
        }

        private void ProcessDB(ITemplateBuilder templateBuilder, CurrencyInfo ci, DBParser db)
        {
            var callSection = db.GetSection(ci.CallSection);
            var putSection = db.GetSection(ci.PutSection);

            var minOi = IgnoreOiThreshold ? null : db.MinOi[ci.ForexName];
            var maxOi = IgnoreOiThreshold ? null : db.MaxOi[ci.ForexName];

            var callContract = GetContract(ci.CallSection, ci.CallContractName, db);
            var putContract = GetContract(ci.PutSection, ci.PutContractName, db);

            var tf = new StrikeFilter(this, db, ci);

            if (callContract != null)
            {
                var calls = callContract.Strikes.FindAll(tf.Filter)
                    .ConvertAll(s => new OptLevel(s, ci, true));

                float maxLength = 0;
                float? sec;
                foreach (var call in calls)
                {
                    var l = LevelLength(call, this.LevelStyle, out sec);
                    if (sec == null) sec = Math.Max(maxLength, l);
                    maxLength = Math.Max(Math.Max(sec.Value, l), maxLength);
                }

                foreach (var call in calls)
                    CreateLevel(templateBuilder, call, db, maxLength, "COL_CALL_BRIGHT", "COL_CALL_DARK", ignoreSettPrice ? StrikeMode.Left : StrikeMode.None);
            }

            if (putContract != null)
            {
                var puts = putContract.Strikes.FindAll(tf.Filter)
                    .ConvertAll(s => new OptLevel(s, ci, false));

                float maxLength = 0;
                float? sec;

                foreach (var put in puts)
                {
                    var l = LevelLength(put, this.LevelStyle, out sec);
                    if (sec == null) sec = Math.Max(maxLength, l);
                    maxLength = Math.Max(Math.Max(sec.Value, l), maxLength);
                }

                foreach (var put in puts)
                    CreateLevel(templateBuilder, put, db, maxLength, "COL_PUT_BRIGHT", "COL_PUT_DARK", ignoreSettPrice ? StrikeMode.Right : StrikeMode.None);
            }
          
        }

        enum StrikeMode
        {
            None,
            Left,
            Right
        }

        private void CreateLevel(ITemplateBuilder templateBuilder, OptLevel level, DBParser db, float maxLength, string brightTemplate, string darkTemplate, StrikeMode strikeMode)
        {
            float? sec;
            float llen;
            llen = LevelLength(level, this.LevelStyle, out sec);

            if (!sec.HasValue) sec = maxLength;

            var start = strikeMode == StrikeMode.Right ? db.LevelDate.AddHours(12) : db.LevelDate;

            Action<float, string> CreateLine = (length, template) =>
            {
                if (maxLength == 0) return;

                var description = this.LevelDescriptionFormat
                    .Replace("_OI_", level.Strike.OpenInterest.ToString())
                    .Replace("_OICH_", (level.Strike.OpenInterestChange >= 0 ? "+" : "") + level.Strike.OpenInterestChange)
                    .Replace("_VOL_", level.Strike.VolumeTradesCleared.ToString())
                    .Replace("_PT_", level.Strike.SettPrice.ToString())
                    .Replace("_LV_", (templateBuilder.Level2Spot(level.Level, db) / 1.000000000000000000000000000000000m).ToString()) // http://stackoverflow.com/questions/4525854/remove-trailing-zeros
                    .Replace("_ST_", level.Strike.StrikeValue.ToString())
                    .Replace("_DT_", level.Strike.Delta.ToString())
                    .Replace("_EX_", level.Strike.Exercises.ToString());

                var l = strikeMode == StrikeMode.None ? level.Level : level.StrikeLevel;

                templateBuilder.AddLevel(
                    l, l, start, db,
                    strikeMode == StrikeMode.None ? (length / maxLength) : (length / maxLength) / 2,
                    level.Strike.StrikeValue.ToString(),
                    description,
                    template);
            };

            if (llen <= sec)
            {
                CreateLine(sec.Value, darkTemplate);
                CreateLine(llen, brightTemplate);
            }
            else
            {
                CreateLine(llen, brightTemplate);
                CreateLine(sec.Value, darkTemplate);
            }
        }

        private float LevelLength(OptLevel level, ClassicOptionLevelStyle style, out float? secondary)
        {
            secondary = null;
            switch (style)
            {
                case ClassicOptionLevelStyle.Normal:
                    return 1f;
                case ClassicOptionLevelStyle.Volumes:
                    return (float)(level.Strike.VolumeTradesCleared);
                case ClassicOptionLevelStyle.OpenInterest:
                    return (float)(level.Strike.OpenInterest);
                case ClassicOptionLevelStyle.OiAndOiCh:
                    secondary = (float)(level.Strike.OpenInterest + level.Strike.OpenInterestChange);
                    return (float)(level.Strike.OpenInterest);
                case ClassicOptionLevelStyle.OpenInterestSetPrice:
                    return (float)(level.Strike.OpenInterest * level.Strike.SettPrice);
                case ClassicOptionLevelStyle.VolumesSetPrice:
                    return (float)(level.Strike.VolumeTradesCleared * level.Strike.SettPrice);
                default:
                    throw new NotImplementedException();
            }
        }

        private int TextLevelsSortComparison(CMEDB.Strike a, CMEDB.Strike b)
        {
            int res = 0;
            switch (this.TextLevelsSortMode)
            {
                case ClassicOptionTextLevelsSort.OpenInterest:
                    res = a.OpenInterest.CompareTo(b.OpenInterest); break;
                case ClassicOptionTextLevelsSort.Volume:
                    res = a.VolumeTradesCleared.CompareTo(b.VolumeTradesCleared); break;
                case ClassicOptionTextLevelsSort.Level:
                    res = a.StrikeValue.CompareTo(b.StrikeValue); break;
                default:
                    break;
            }

            return this.TextLevelsReverseOrder ? -res : res;
        }
        
        private ClassicOptionTextLevelsSort textLevelsSortMode = ClassicOptionTextLevelsSort.Level;
        [Category("Уровни текстом"), DisplayName("Порядок сортировки"), Description("Столбец, по которому будет выполняться сортировка"), DefaultValue(ClassicOptionTextLevelsSort.Level)]
        public ClassicOptionTextLevelsSort TextLevelsSortMode
        {
            get
            {
                return textLevelsSortMode;
            }
            set
            {
                if (textLevelsSortMode != value)
                {
                    textLevelsSortMode = value;
                    OnPropertyChanged("TextLevelsSortMode");
                }
            }
        }


        private bool textLevelsReverseOrder = false;
        [Category("Уровни текстом"), DisplayName("Обратный порядок"), Description("Сортировать уровни от большего к меньшему"), DefaultValue(false)]
        public bool TextLevelsReverseOrder
        {
            get
            {
                return textLevelsReverseOrder;
            }
            set
            {
                if (textLevelsReverseOrder != value)
                {
                    textLevelsReverseOrder = value;
                    OnPropertyChanged("TextLevelsReverseOrder");
                }
            }
        }


        private bool hideCab = true;
        [DisplayName("Скрыть CAB")]
        [Description("Значение true (истина) означает, что CAB-страйки будут скрыты")]
        [DefaultValue(true)]
        public bool HideCab
        {
            get
            {
                return hideCab;
            }
            set
            {
                if (hideCab != value)
                {
                    hideCab = value;
                    OnPropertyChanged("HideCab");
                }
            }
        }

        private ClassicOptionLevelStyle levelStyle = ClassicOptionLevelStyle.Normal;

        [DisplayName("Стиль уровней")]
        [Description("Определяет способ визуализации параметров страйка")]
        [DefaultValue(ClassicOptionLevelStyle.Normal)]
        public ClassicOptionLevelStyle LevelStyle
        {
            get
            {
                return levelStyle;
            }
            set
            {
                if (this.levelStyle != value)
                {
                    this.levelStyle = value;
                    OnPropertyChanged("LevelStyle");
                }
            }
        }

        private bool ignoreOiThreshold = false;
        [DisplayName("Игнорировать пороги ОИ")]
        [Description("Значение true (истина) означает, что не будут учитываться пороговые уровни ОИ")]
        [DefaultValue(false)]
        public bool IgnoreOiThreshold
        {
            get
            {
                return ignoreOiThreshold;
            }
            set
            {
                if (ignoreOiThreshold != value)
                {
                    ignoreOiThreshold = value;
                    OnPropertyChanged("IgnoreOiThreshold");
                }
            }
        }

        private bool ignoreOiChThreshold = false;
        [DisplayName("Игнорировать пороги изменения ОИ")]
        [Description("Значение true (истина) означает, что не будут учитываться пороговые уровни изменения ОИ")]
        [DefaultValue(false)]
        public bool IgnoreOiChThreshold
        {
            get
            {
                return ignoreOiChThreshold;
            }
            set
            {
                if (ignoreOiChThreshold != value)
                {
                    ignoreOiChThreshold = value;
                    OnPropertyChanged("IgnoreOiChThreshold");
                }
            }
        }

        private bool ignoreVolThreshold = false;
        [DisplayName("Игнорировать пороги объёма")]
        [Description("Значение true (истина) означает, что не будут учитываться пороговые уровни объёма")]
        [DefaultValue(false)]
        public bool IgnoreVolThreshold
        {
            get
            {
                return ignoreVolThreshold;
            }
            set
            {
                if (ignoreVolThreshold != value)
                {
                    ignoreVolThreshold = value;
                    OnPropertyChanged("IgnoreVolThreshold");
                }
            }
        }

        DateTime? forContractDate;
        int? forContractNumber = 1;
        static System.Globalization.CultureInfo dateFormat = System.Globalization.CultureInfo.GetCultureInfo("en-us");


        [DisplayName("Контракт")]
        [Description("№ контракта (1 = текущий, 2 следующий и т.д.) или дата экспирации (например NOV13 = ноябрь 2013)")]
        [DefaultValue("1")]
        public string ContractN
        {
            get
            {
                return
                    forContractNumber.HasValue ? forContractNumber.ToString()
                    : forContractDate.HasValue ?
                    forContractDate.Value.ToString("MMMyy", dateFormat).ToUpper()
                    : null;
            }
            set
            {
                int n;
                DateTime dt;

                if (int.TryParse(value, out n))
                {
                    forContractNumber = n;
                    forContractDate = null;
                }
                else if (DateTime.TryParse("01" + value, out dt))
                {
                    forContractNumber = null;
                    forContractDate = dt;
                }
                else
                    throw new Exception("Нужно ввести номер или дату в виде MMMYY");
                OnPropertyChanged("ContractN");
            }
        }

        private bool ignoreSettPrice = false;
        [DisplayName("Не учитывать премию")]
        [Description("Значение true (истина) означает, что уровни будут рисоваться по страйку, без учёта премии")]
        [DefaultValue(false)]
        public bool IgnoreSettPrice
        {
            get
            {
                return ignoreSettPrice;
            }
            set
            {
                if (ignoreSettPrice != value)
                {
                    ignoreSettPrice = value;
                    OnPropertyChanged("IgnoreSettPrice");
                }
            }
        }



        private string levelDescriptionFormat = "_OI_ _OICH_ V=_VOL_ Pt=_PT_";
        [DisplayName("Формат подписи уровня")]
        [Description("OI = открытый интерес, OICH = изменение ОИ, VOL = объём, PT = премия, LV = уровень, ST = номер страйка, DT = дельта, EX = Exercises")]
        [DefaultValue("_OI_ _OICH_ V=_VOL_ Pt=_PT_")]
        public string LevelDescriptionFormat
        {
            get
            {
                return levelDescriptionFormat;
            }
            set
            {
                if (levelDescriptionFormat != value)
                {
                    levelDescriptionFormat = value;
                    OnPropertyChanged("LevelDescriptionFormat");
                }
            }
        }

        public override void SaveSettings(XmlElement settings)
        {
            settings.SetAttribute("TextLevelsSortMode", Enum.GetName(typeof(ClassicOptionTextLevelsSort), this.TextLevelsSortMode));
            settings.SetAttribute("TextLevelsReverseOrder", this.textLevelsReverseOrder.ToString());

            settings.SetAttribute("LevelStyle", Enum.GetName(typeof(ClassicOptionLevelStyle), this.LevelStyle));
            settings.SetAttribute("HideCab", this.HideCab.ToString());
            settings.SetAttribute("IgnoreOiThreshold", this.IgnoreOiThreshold.ToString());
            settings.SetAttribute("IgnoreOiChThreshold", this.IgnoreOiChThreshold.ToString());
            settings.SetAttribute("IgnoreVolThreshold", this.IgnoreVolThreshold.ToString());
            settings.SetAttribute("ContractN", this.ContractN);
            settings.SetAttribute("IgnoreSettPrice", this.IgnoreSettPrice.ToString());
            settings.SetAttribute("LevelDescriptionFormat", this.LevelDescriptionFormat);
        }

        public override void LoadSettings(XmlElement genSettings)
        {
            if (genSettings.HasAttribute("TextLevelsSortMode"))
                this.TextLevelsSortMode = (ClassicOptionTextLevelsSort)Enum.Parse(typeof(ClassicOptionTextLevelsSort), genSettings.GetAttribute("TextLevelsSortMode"));
            if (genSettings.HasAttribute("TextLevelsReverseOrder"))
                TextLevelsReverseOrder = bool.Parse(genSettings.GetAttribute("TextLevelsReverseOrder"));


            if (genSettings.HasAttribute("LevelStyle"))
                LevelStyle = (ClassicOptionLevelStyle)Enum.Parse(typeof(ClassicOptionLevelStyle), genSettings.GetAttribute("LevelStyle"));
            if (genSettings.HasAttribute("HideCab"))
                HideCab = bool.Parse(genSettings.GetAttribute("HideCab"));
            if (genSettings.HasAttribute("IgnoreOiThreshold"))
                IgnoreOiThreshold = bool.Parse(genSettings.GetAttribute("IgnoreOiThreshold"));
            if (genSettings.HasAttribute("IgnoreOiChThreshold"))
                IgnoreOiChThreshold = bool.Parse(genSettings.GetAttribute("IgnoreOiChThreshold"));
            if (genSettings.HasAttribute("IgnoreVolThreshold"))
                IgnoreVolThreshold = bool.Parse(genSettings.GetAttribute("IgnoreVolThreshold"));
            if (genSettings.HasAttribute("ContractN"))
                ContractN = genSettings.GetAttribute("ContractN");
            if (genSettings.HasAttribute("IgnoreSettPrice"))
                IgnoreSettPrice = bool.Parse(genSettings.GetAttribute("IgnoreSettPrice"));
            if (genSettings.HasAttribute("LevelDescriptionFormat"))
                LevelDescriptionFormat = genSettings.GetAttribute("LevelDescriptionFormat");
        }
    }

    public enum ClassicOptionTextLevelsSort
    {
        [FieldDisplayName("Открытый интерес")]
        OpenInterest,
        [FieldDisplayName("Объём")]
        Volume,
        [FieldDisplayName("Уровень")]
        Level,
    }

    public enum ClassicOptionLevelStyle
    {
        [FieldDisplayName("простая линия")]
        Normal,
        [FieldDisplayName("объём")]
        Volumes,
        [FieldDisplayName("открытый интерес")]
        OpenInterest,
        [FieldDisplayName("изменение ОИ")]
        OiAndOiCh,
        [FieldDisplayName("открытый интерес * премию")]
        OpenInterestSetPrice,
        [FieldDisplayName("объём * премию")]
        VolumesSetPrice,
    }
}
