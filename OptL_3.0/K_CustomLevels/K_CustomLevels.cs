﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using OptL;
using CMEDB;
using System.ComponentModel;

namespace OpenInterestIndicator
{
    public class K_CustomLevels : LevelsGenerator
    {
        string[] csvContent = null;

        public override void ShowUI(CurrencyInfo ci, bool spot, IEnumerable<DBParser> dbs)
        {
            csvContent = null;

            var files = new Dictionary<string, string>();

            if (!string.IsNullOrWhiteSpace(DataFiles))
            {
                foreach (var dataFilePath in DataFiles.Split(';'))
                {
                    foreach (var ci2 in CurrencyList)
                    {
                        if (System.IO.Path.GetFileName(dataFilePath).StartsWith(ci2.ForexName, StringComparison.InvariantCultureIgnoreCase))
                            files.Add(ci2.ForexName, dataFilePath);
                    }
                }
            }

            var fs = new System.Windows.Forms.OpenFileDialog();
            fs.CheckPathExists = true;
            fs.Filter = "Excel файл CSV|" + ci.ForexName + "*.csv";
            fs.Title = ToString();
            fs.Multiselect = false;
            if (files.ContainsKey(ci.ForexName))
                fs.FileName = files[ci.ForexName];

            if (fs.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (files.ContainsKey(ci.ForexName))
                    files[ci.ForexName] = fs.FileName;
                else
                    files.Add(ci.ForexName, fs.FileName);
            }

            if (files.ContainsKey(ci.ForexName))
                csvContent = System.IO.File.ReadAllLines(files[ci.ForexName]);

            DataFiles = string.Join(";", files.Values);
        }

        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            int N = 0;
            foreach (var line in csvContent)
            {
                N++;
                if (string.IsNullOrWhiteSpace(line))
                    continue;
                var cols = line.Split(';');
                if (cols.Length < 3)
                    throw new Exception("Неверное кол-во колонок в строке " + N.ToString());
                var date = DateTime.Parse(cols[0]);
                var templateName = cols[1];
                var q1 = decimal.Parse(cols[2]);
                var q2 = cols.Length < 4 || string.IsNullOrWhiteSpace(cols[3]) ? q1 : decimal.Parse(cols[3]);
                var text = cols.Length < 5 ? "" : cols[4];

                string objectType;
                try
                {
                    objectType = templateBuilder.GetObjectType(templateName);
                }
                catch (KeyNotFoundException)
                {
                    throw new LevelGeneratorException("Уровень с именем '" + templateName + "' не найден в optl.tpl");
                }

                if (objectType == "2" || objectType == "16")
                    templateBuilder.AddLevel(q1, q2, date, date.AddMinutes(levelLength), null, "", text, templateName);
                else if (objectType == "22")
                {
                    int code;
                    if (!int.TryParse(text, out code))
                        throw new Exception("Код стрелки должен быть числом, ошибка в строке " + N.ToString());
                    templateBuilder.AddArrow(q1, date, null, "", text, templateName, int.Parse(text));
                }
            }
        }

        public void GenerateDayLevels(ITemplateBuilder templateBuilder, CurrencyInfo ci, DBParser l, decimal dayOpenPrice)
        {
        }

        private string dataFiles = "";
        [DisplayName("Пути к файлам данных")]
        [Description("")]
        [DefaultValue(false)]
        public string DataFiles
        {
            get
            {
                return dataFiles;
            }
            set
            {
                if (dataFiles != value)
                {
                    dataFiles = value;
                    OnPropertyChanged("DataFiles");
                }
            }
        }

        private int levelLength = 1440;
        [DisplayName("Длина уровня")]
        [Description("Длина уровня в минутах (1440 = 1 день)")]
        [DefaultValue(1440)]
        public int LevelLength
        {
            get
            {
                return levelLength;
            }
            set
            {
                if (levelLength != value)
                {
                    levelLength = value;
                    OnPropertyChanged("LevelLength");
                }
            }
        }

        public override void SaveSettings(System.Xml.XmlElement settings)
        {
            settings.SetAttribute("DataFiles", dataFiles);
            settings.SetAttribute("LevelLength", levelLength.ToString());
        }

        public override void LoadSettings(System.Xml.XmlElement genSettings)
        {
            if (genSettings.HasAttribute("DataFiles"))
                this.DataFiles = genSettings.GetAttribute("DataFiles");
            if (genSettings.HasAttribute("LevelLength"))
                this.LevelLength = int.Parse(genSettings.GetAttribute("LevelLength"));
        }

        public override bool WritesTemplate
        {
            get
            {
                return true;
            }
        }

        public override string ToString()
        {
            return "Уровни из CSV";
        }
    }
}
