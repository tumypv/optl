﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace OptL
{
    public partial class CacheLoadForm : Form
    {
        string cachePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "cache.bin");
        public List<DBParser> Result = new List<DBParser>();
        public List<CurrencyInfo> CIS = new List<CurrencyInfo>();

        public CacheLoadForm()
        {
            InitializeComponent();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            using (var file = File.OpenRead(cachePath))
            {
                //var xrs = new XmlReaderSettings();
                //xrs.IgnoreWhitespace = true;

                //var xr = XmlReader.Create(file, xrs);
                var xr = new BinaryXMLReader(file);
                xr.MoveToContent();

                if (!xr.IsEmptyElement)
                {
                    
                    xr.Read();
                    while (xr.Read() && xr.NodeType != XmlNodeType.EndElement)
                    {
                        var ciName = xr.GetAttribute("Name");
                        var ci = CurrencyInfo.AvailableCurrencyInfoList.Find(_ci => _ci.ForexName == ciName);
                        CIS.Add(ci);
                    }
                    //xr.ReadEndElement();


                    while (xr.Read() && xr.NodeType != XmlNodeType.EndElement)
                    {
                        var db = DBParser.Load(xr.ReadSubtree());
                        Result.Add(db);
                        if (backgroundWorker1.CancellationPending)
                            break;
                        backgroundWorker1.ReportProgress((int)((float)file.Position * 100 / file.Length), db);
                    }
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            label2.Text = "Всего " + Result.Count + ", последний " +  (e.UserState as DBParser).FileName; 
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.DialogResult = e.Cancelled ?
                System.Windows.Forms.DialogResult.Cancel : System.Windows.Forms.DialogResult.OK;
        }

        internal bool Start()
        {
            if (File.Exists(cachePath))
            {
                backgroundWorker1.RunWorkerAsync();
                return true;
            }
            else
                return false;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }
    }
}
