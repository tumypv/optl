﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using MT4Tools;
using System.ComponentModel;
using System.Xml;
using CMEDB;

namespace OptL
{
    //[LevelsGeneratorSettingsEditor("OptL.ClassicOptionLevelsGeneratorSettingsEditor")]
    public class ClassicOptionLevelsGenerator2 : LevelsGenerator2
    {
        public override string ToString()
        {
            return "Исторические уровни";
        }

        public override bool WritesTemplate
        {
            get
            {
                return CreateTemplate;
            }
        }

        class History
        {
            internal List<DBParser> dbs;
            public CurrencyInfo ci { get; private set; }

            public History(IEnumerable<DBParser> dbs, CurrencyInfo ci)
            {
                this.ci = ci;
                this.dbs = new List<DBParser>(dbs);
                this.dbs.Sort((a, b) => a.Date.CompareTo(b.Date));
            }

            public Day LastDay
            {
                get
                {
                    return new Day(dbs[dbs.Count - 1], this);
                }
            }

            public IEnumerable<Day> Days
            {
                get
                {
                    foreach (var db in dbs)
                    {
                        yield return new Day(db, this);
                    }
                }
            }

            public IEnumerable<StrikeHistoryItem> GetStrikeHistory(int Strike, DateTime ExpirationDate, DBParser lastDB)
            {
                foreach (var db in dbs)
                {
                    var res = GetHistoryItem(Strike, ExpirationDate, db);

                    yield return res;

                    if (db == lastDB)
                        break;
                }
            }

            internal StrikeHistoryItem GetHistoryItem(int Strike, DateTime ExpirationDate, DBParser db)
            {
                var callName = ci.CallContractName;
                var putName = ci.PutContractName;
                var ed = ExpirationDate;
                var callContract = db.GetSection(ci.CallSection).Contracts.Find(c => c.Name.StartsWith(callName) && c.Date == ed);
                var putContract = db.GetSection(ci.PutSection).Contracts.Find(c => c.Name.StartsWith(putName) && c.Date == ed);

                var res = new StrikeHistoryItem();
                if (callContract != null)
                    res.Call = callContract.Strikes.Find(s => s.StrikeValue == Strike);
                if (putContract != null)
                    res.Put = putContract.Strikes.Find(s => s.StrikeValue == Strike);

                res.DB = db;
                return res;
            }
        }

        struct Day
        {
            private DBParser db;
            private History history;

            public Day(DBParser db, History history)
            {
                this.db = db;
                this.history = history;
            }

            public IEnumerable<Contract> Contracts
            {
                get
                {
                    var callName = history.ci.CallContractName;
                    var putName = history.ci.PutContractName;
                    var calls = db.GetSection(history.ci.CallSection).Contracts.FindAll(c => c.Name.StartsWith(callName));
                    var puts = db.GetSection(history.ci.PutSection).Contracts.FindAll(c => c.Name.StartsWith(putName));

                    var contracts = new List<DateTime>();

                    foreach (var call in calls)
                        contracts.Add(call.Date);

                    foreach (var put in calls)
                        if (!contracts.Contains(put.Date))
                            contracts.Add(put.Date);

                    contracts.Sort();

                    for (int i = 0; i < contracts.Count; i++)
                        yield return new Contract(contracts[i], i + 1, db, history);
                }
            }

            public DBParser DB
            {
                get
                {
                    return db;
                }
            }
        }

        struct Contract
        {
            private DBParser db;
            private History history;
            public readonly DateTime ExpirationDate;
            public readonly int N;

            public Contract(DateTime expirationDate, int n, DBParser db, History history)
            {
                this.db = db;
                this.history = history;
                this.ExpirationDate = expirationDate;
                this.N = n;
            }

            public IEnumerable<StrikeHistory> Strikes
            {
                get
                {
                    var callName = history.ci.CallContractName;
                    var putName = history.ci.PutContractName;
                    var ed = ExpirationDate;
                    var callContract = db.GetSection(history.ci.CallSection).Contracts.Find(c => c.Name.StartsWith(callName) && c.Date == ed);
                    var putContract = db.GetSection(history.ci.PutSection).Contracts.Find(c => c.Name.StartsWith(putName) && c.Date == ed);

                    var strikes = new List<int>();
                    if (callContract != null)
                        strikes.AddRange(callContract.Strikes.ConvertAll(s => s.StrikeValue));
                    if (putContract != null)
                        strikes.AddRange(putContract.Strikes.ConvertAll(s => s.StrikeValue));
                    strikes.Sort();
                    int last = -1;
                    foreach (var strike in strikes)
                    {
                        if (strike != last)
                            yield return new StrikeHistory(strike, ExpirationDate, db, history);
                        last = strike;
                    }
                   
                }
            }
        }

        struct StrikeHistory
        {
            private DateTime ExpirationDate;
            private DBParser db;
            private History history;
            public readonly int Strike;
            
            public StrikeHistory(int strike, DateTime ExpirationDate, DBParser db, History history)
            {
                this.Strike = strike;
                this.ExpirationDate = ExpirationDate;
                this.db = db;
                this.history = history;
            }

            public IEnumerable<StrikeHistoryItem> History
            {
                get
                {
                    return history.GetStrikeHistory(Strike, ExpirationDate, db);
                }
            }

            public StrikeHistoryItem Last
            {
                get
                {
                    return history.GetHistoryItem(Strike, ExpirationDate, db);
                }
            }
        }

        struct StrikeHistoryItem
        {
            public DBParser DB { get; set; }
            public Strike Call { get; set; }
            public Strike Put { get; set; }
        }

        public override IEnumerable<LevelsGeneratorAdditionalFile> GenerateTemplate(IEnumerable<DBParser> dbs, CurrencyInfo ci, ITemplateBuilder templateBuilder)
        {
            var history = new History(dbs, ci);

            foreach (var day in history.Days)
            {
                List<SInfo> strikes;

                strikes = ProcessDay(day);

                int maxLCall = 0, maxLPut = 0, maxL = 0;
                strikes.FindAll(s => s.isCall).ForEach(s => maxLCall = Math.Max(maxLCall, Math.Max(s.OpenInterest, s.Last.OpenInterest - s.Last.OpenInterestChange)));
                strikes.FindAll(s => !s.isCall).ForEach(s => maxLPut = Math.Max(maxLPut, Math.Max(s.OpenInterest, s.Last.OpenInterest - s.Last.OpenInterestChange)));
                maxL = Math.Max(maxLCall, maxLPut);

                if (maxL == 0)
                    continue;

                decimal callTotal = 0, putTotal = 0;
                int callTotalOi = 0, putTotalOi = 0;

                foreach (var strike in strikes)
                {
                    var s = strike.Last;

                    var level = s.StrikeValue * ci.StrikeMultiplier + (strike.isCall ? 1 : -1) * (strike.settPriceWeighted / strike.OpenInterest) * ci.SettPriceMultiplier;

                    string descr = s.OpenInterest + (s.OpenInterestChange >= 0 ? "+" : "") + s.OpenInterestChange.ToString();

                    var oiNew = (float)s.OpenInterest / maxL;
                    var oiOld = (float)(s.OpenInterest - s.OpenInterestChange) / maxL;

                    var name = (strike.isCall ? "C" : "P") + s.StrikeValue;

                    if (oiNew > oiOld)
                    {
                        var template = strike.isCall ? "COL_CALL_DARK" : "COL_PUT_DARK";
                        templateBuilder.AddLevel(level, level, day.DB.LevelDate, day.DB, oiNew, name, descr, template);
                        template = strike.isCall ? "COL_CALL_BRIGHT" : "COL_PUT_BRIGHT";
                        templateBuilder.AddLevel(level, level, day.DB.LevelDate, day.DB, oiOld, name, descr, template);
                    }
                    else
                    {
                        var template = strike.isCall ? "COL_CALL_BRIGHT" : "COL_PUT_BRIGHT";
                        templateBuilder.AddLevel(level, level, day.DB.LevelDate, day.DB, oiOld, name, descr, template);
                        template = strike.isCall ? "COL_CALL_DARK" : "COL_PUT_DARK";
                        templateBuilder.AddLevel(level, level, day.DB.LevelDate, day.DB, oiNew, name, descr, template);
                    }

                    if (strike.isCall)
                    {
                        callTotal += level * s.OpenInterest;
                        callTotalOi += s.OpenInterest;
                    }
                    else
                    {
                        putTotal += level * s.OpenInterest;
                        putTotalOi += s.OpenInterest;
                    }
                }

                if (putTotalOi > 0 && callTotalOi > 0)
                {
                    var putL = putTotal / putTotalOi;
                    templateBuilder.AddLevel(putL, putL, day.DB.LevelDate, day.DB, 1f, "PR_DAY_TARGET", "PUT", "PR_DAY_TARGET");
                    var callL = callTotal / callTotalOi;
                    templateBuilder.AddLevel(callL, callL, day.DB.LevelDate, day.DB, 1f, "K2_TOP2", "CALL", "K2_TOP2");
                    var L = (putTotal + callTotal) / (putTotalOi + callTotalOi);
                    templateBuilder.AddLevel(L, L, day.DB.LevelDate, day.DB, 1f, "K2_BALANCEY", "PUT+CALL", "K2_BALANCEY");
                }
            }

            if (SaveCSV != ClassicOptionLevelStyle.NoExport)
            {
                var sb = CreateCSV(history);
                yield return
                    new LevelsGeneratorAdditionalFile()
                    {
                        Name = string.Format("{0} Исторические уровни.csv", ci.ForexName),
                        Data = Encoding.UTF8.GetBytes(sb.ToString())
                    };
            }
        }

        private List<SInfo> ProcessDay(Day day)
        {
            List<SInfo> strikes = new List<SInfo>();

            foreach (var c in day.Contracts)
            {
                if (SkipContract(c)) continue;

                foreach (var s in c.Strikes)
                {
                    SInfo callInfo, putInfo;
                    CalcHistory(s, out callInfo, out putInfo);

                    if (callInfo != null)
                        strikes.Add(callInfo);
                    if (putInfo != null)
                        strikes.Add(putInfo);
                }
            }
            return strikes;
        }

        private bool SkipContract(Contract c)
        {
            return (forContractNumber.HasValue && c.N != forContractNumber) || (forContractDate.HasValue && c.ExpirationDate != forContractDate);
        }

        private StringBuilder CreateCSV(History history)
        {
            var sb = new StringBuilder();

            foreach (var day in history.Days)
            {
                bool export = 
                    SaveCSV == ClassicOptionLevelStyle.All 
                    || (SaveCSV == ClassicOptionLevelStyle.OnlyLast && history.LastDay.DB == day.DB);

                foreach (var c in day.Contracts)
                {
                    if (SkipContract(c)) continue;

                    if (export)
                    {
                        sb.AppendLine(";;;;;;;DATE;" + day.DB.Date.ToShortDateString());
                        sb.AppendLine(";;;;;;;;OPTIONS");
                        sb.AppendLine(";;;;;;;;" + c.ExpirationDate.ToString("MMMM yyyy", System.Globalization.CultureInfo.InvariantCulture));
                        sb.AppendLine(";;CALL;;;;;;STRIKE PRICE;;;PUT");
                        sb.Append("VOLUME TRADES CLEARED;OPEN INTEREST;OPEN INTEREST CHANGE;SETT.PRICE;H_SETT.PRICE;AskOPEN INTEREST;BidOPEN INTEREST;DELTA");
                        sb.Append(";;");
                        sb.AppendLine("VOLUME TRADES CLEARED;OPEN INTEREST;OPEN INTEREST CHANGE;SETT.PRICE;H_SETT.PRICE;AskOPEN INTEREST;BidOPEN INTEREST;DELTA");
                    }

                    int c_tv = 0, c_toi = 0, c_toich = 0;
                    int p_tv = 0, p_toi = 0, p_toich = 0;

                    foreach (var s in c.Strikes)
                    {
                        SInfo callInfo, putInfo;
                        CalcHistory(s, out callInfo, out putInfo);

                        var last = s.Last;

                        if (last.Call != null)
                        {
                            c_tv += last.Call.VolumeTradesCleared;
                            c_toi += last.Call.OpenInterest;
                            c_toich += last.Call.OpenInterestChange;
                        }

                        if (last.Put != null)
                        {
                            p_tv += last.Put.VolumeTradesCleared;
                            p_toi += last.Put.OpenInterest;
                            p_toich += last.Put.OpenInterestChange;
                        }

                        if (export)
                        {
                            sb.Append(FormatStrike(last.Call, callInfo));
                            sb.Append(";");
                            sb.Append(s.Strike);
                            sb.Append(";");
                            sb.AppendLine(FormatStrike(last.Put, putInfo));
                        }
                    }
                    if (export)
                    {
                        sb.AppendLine(string.Format("{0};{1};{2};;;;;;TOTAL;{3};{4};{5}", c_tv, c_toi, c_toich, p_tv, p_toi, p_toich));
                        sb.AppendLine();
                    }
                }
            }
            return sb;
        }

        private void CalcHistory(StrikeHistory s, out SInfo callInfo, out SInfo putInfo)
        {
            callInfo = null;
            putInfo = null;

            var sh = new List<StrikeHistoryItem>(s.History);

            foreach (var h in s.History)
            {
                if (h.Call != null)
                {
                    if (callInfo == null)
                        callInfo = new SInfo(true);
                    callInfo.Add(h.Call);
                    if (callInfo.OpenInterest == 0)
                        callInfo = null;
                }

                if (h.Put != null)
                {
                    if (putInfo == null)
                        putInfo = new SInfo(false);
                    putInfo.Add(h.Put);
                    if (putInfo.OpenInterest == 0)
                        putInfo = null;
                }
            }
        }

        class SInfo
        {
            //StringBuilder sb = new StringBuilder();

            public int OpenInterest;
            public decimal settPriceWeighted;

            public decimal Ask;
            public decimal Bid;

            public Strike Last {get; private set;}

            public bool isCall { get; private set; }

            public decimal SettPrice
            {
                get
                {
                    return settPriceWeighted / OpenInterest;
                }
            }

            public SInfo(bool isCall)
            {
                this.isCall = isCall;
            }

            internal void Add(Strike s)
            {
              //sb.AppendLine(s.OpenInterest +"\t"+ s.SettPrice+"\t"+s.OpenInterestChange);

                settPriceWeighted += (s.OpenInterest - OpenInterest) * s.SettPrice;
                OpenInterest = s.OpenInterest;

                if (s.OpenInterestChange > 0)
                {
                    if (s.SettPrice < 0.9m)
                        Ask += s.OpenInterestChange;
                    else if (s.SettPrice > 0.9m)
                        Bid += s.OpenInterestChange;
                }
                else if (s.OpenInterestChange < 0)
                {
                    if (s.SettPrice < 0.9m)
                        Bid += s.OpenInterestChange;
                    else if (s.SettPrice > 0.9m)
                        Ask += s.OpenInterestChange;
                }
                
                this.Last = s;
            }
        }

        private string FormatStrike(Strike strike, SInfo si)
        {
            if (strike != null)
            {

                return string.Format("{0};{1};{2};{3};{4:.00};{5};{6};{7}",
                    strike.VolumeTradesCleared,
                    strike.OpenInterest,
                    strike.OpenInterestChange,
                    strike.SettPrice,
                    si == null ? "----" : (si.OpenInterest > 0 ? si.settPriceWeighted / si.OpenInterest : strike.SettPrice).ToString(),
                    si == null ? "----" : si.Ask.ToString(),
                    si == null ? "----" : si.Bid.ToString(),
                    strike.Delta);
            }
            else
                return (";;;;;;;");
        }

        private ClassicOptionLevelStyle saveCSV = ClassicOptionLevelStyle.NoExport;
        [DisplayName("Сохранять в файл")]
        [Description("Кроме создания шаблона, также выполнять сохранение в CSV для Excel")]
        [DefaultValue(false)]
        public ClassicOptionLevelStyle SaveCSV
        {
            get
            {
                return saveCSV;
            }
            set
            {
                if (saveCSV != value)
                {
                    saveCSV = value;
                    OnPropertyChanged("SaveCSV");
                }
            }
        }

        private bool createTemplate = true;
        [DisplayName("Создавать шаблон")]
        [Description("")]
        [DefaultValue(true)]
        public bool CreateTemplate
        {
            get
            {
                return createTemplate;
            }
            set
            {
                if (createTemplate != value)
                {
                    createTemplate = value;
                    OnPropertyChanged("CreateTemplate");
                }
            }
        }

        DateTime? forContractDate = null;
        int? forContractNumber = 1;
        static System.Globalization.CultureInfo dateFormat = System.Globalization.CultureInfo.GetCultureInfo("en-us");

        [DisplayName("Контракт")]
        [Description("№ контракта (1 = текущий, 2 следующий и т.д.) или дата экспирации (например NOV13 = ноябрь 2013)")]
        [DefaultValue("1")]
        public string ContractN
        {
            get
            {
                return 
                    forContractNumber.HasValue ? forContractNumber.ToString() 
                    : forContractDate.HasValue ? 
                    forContractDate.Value.ToString("MMMyy", dateFormat).ToUpper()
                    : "???";
            }
            set
            {
                int n;
                DateTime dt;

                if (int.TryParse(value, out n))
                {
                    forContractNumber = n;
                    forContractDate = null;
                }
                else if (DateTime.TryParse("01"+value, out dt))
                {
                    forContractNumber = null;
                    forContractDate = dt;
                }
                else
                    throw new Exception("Нужно ввести номер или дату в виде MMMYY");
                OnPropertyChanged("ContractN");
            }
        }

        public override void SaveSettings(XmlElement settings)
        {
            settings.SetAttribute("SaveCSV", this.SaveCSV.ToString());
            settings.SetAttribute("ContractN", this.ContractN);
            settings.SetAttribute("CreateTemplate", this.CreateTemplate.ToString());
        }

        public override void LoadSettings(XmlElement genSettings)
        {
            if (genSettings.HasAttribute("SaveCSV"))
                SaveCSV = (ClassicOptionLevelStyle)Enum.Parse(typeof(ClassicOptionLevelStyle), genSettings.GetAttribute("SaveCSV"));

            if (genSettings.HasAttribute("ContractN"))
                ContractN = genSettings.GetAttribute("ContractN");

            if (genSettings.HasAttribute("CreateTemplate"))
                CreateTemplate = bool.Parse(genSettings.GetAttribute("CreateTemplate"));
        }
    }

    public enum ClassicOptionLevelStyle
    {
        [FieldDisplayName("не сохранять")]
        NoExport,
        [FieldDisplayName("только последний")]
        OnlyLast,
        [FieldDisplayName("все")]
        All
    }
}
