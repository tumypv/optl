﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace OptL
{
    class BinaryXMLReader : XmlReader
    {
        //XmlReader xr;

        private System.IO.FileStream file;
        Dictionary<short, string> nameTable = new Dictionary<short, string>();
        //Stack<string> openTags = new Stack<string>();
        int depth = 0;
        
        System.IO.BinaryReader br;

        NameTable nt = new NameTable();

        XmlNodeType nodeType = XmlNodeType.None;

        short lastNameId;

        Dictionary<string, string> Attributes = new Dictionary<string, string>();

        //List<KeyValuePair<string, string>> Attributes = new List<KeyValuePair<string, string>>();
        int currentAttribute = -1;

        bool isEmptyElement = false;

        public BinaryXMLReader(System.IO.FileStream file)
        {
            this.file = file;
            br = new System.IO.BinaryReader(file, Encoding.ASCII);
            lastNameId = br.ReadInt16();
            //XmlReaderSettings xrs = new XmlReaderSettings() { IgnoreWhitespace = true };
            //xr = XmlReader.Create(@"C:\PRJ\CME_COT\OptL_2.0\bin\Debug\cache.xml", xrs);
        }

        public override int AttributeCount
        {
            get { throw new NotImplementedException(); }
        }

        public override string BaseURI
        {
            get { throw new NotImplementedException(); }
        }

        public override void Close()
        {
            throw new NotImplementedException();
        }

        public override int Depth
        {
            get
            {
                //if (xr.Depth != openTags.Count-1)
                //    throw new Exception();
                return depth-1;
            }
        }

        public override bool EOF
        {
            get { throw new NotImplementedException(); }
        }

        public override string GetAttribute(int i)
        {
            throw new NotImplementedException();
        }

        public override string GetAttribute(string name, string namespaceURI)
        {
            throw new NotImplementedException();
        }

        public override string GetAttribute(string name)
        {
            //var attr = xr.GetAttribute(name);
            //if (attr != Attributes.Find(a => a.Key == name).Value)
            //    throw new Exception();
            //return Attributes.Find(a => a.Key == name).Value;
            return Attributes[name];
        }

        public override bool HasValue
        {
            get { throw new NotImplementedException(); }
        }

        public override bool IsEmptyElement
        {
            get {
                //if (xr.IsEmptyElement != isEmptyElement)
                //    throw new Exception();
                return isEmptyElement;
            }
        }

        public override string LocalName
        {
            get { throw new NotImplementedException(); }
        }

        public override string LookupNamespace(string prefix)
        {
            throw new NotImplementedException();
        }

        public override bool MoveToAttribute(string name, string ns)
        {
            throw new NotImplementedException();
        }

        public override bool MoveToAttribute(string name)
        {
            throw new NotImplementedException();
        }

        public override bool MoveToElement()
        {
            //var res = xr.MoveToElement();

            if (currentAttribute == -1)
            {
                //if (res) throw new Exception();
                return false;
            }
            else
            {
                //if (!res) throw new Exception();
                currentAttribute = -1;
                return true;
            }
        }

        public override bool MoveToFirstAttribute()
        {
            //var res = xr.MoveToFirstAttribute();
            if (Attributes.Count == 0)
            {
                //if (res) throw new Exception();
                return false;
            }
            else
            {
                //if (!res) throw new Exception();
                currentAttribute = 0;
                return true;
            }
        }

        public override bool MoveToNextAttribute()
        {
            //var res = xr.MoveToNextAttribute();
            if (currentAttribute + 1 < Attributes.Count)
            {
                //if (!res) throw new Exception();
                currentAttribute++;
                return true;
            }
            else
            {
                //if (res) throw new Exception();
                return false;
            }
        }

        public override XmlNameTable NameTable
        {
            get { return this.nt; }
        }

        public override string NamespaceURI
        {
            get { return ""; }
        }

        public override XmlNodeType NodeType
        {
            get 
            {
                //if (nodeType != xr.NodeType) throw new Exception();
                return nodeType;
            }
        }

        public override string Prefix
        {
            get { return ""; }
        }

        public override bool Read()
        {
            //var res = xr.Read();
            //if (xr.NodeType == XmlNodeType.XmlDeclaration)
            //    res = xr.Read();

            Attributes.Clear();
            currentAttribute = -1;

            if (lastNameId < 0)
                throw new Exception();
            else if (lastNameId == 0)
            {
                if (br.PeekChar() == -1) return false;
                lastNameId = br.ReadInt16();
                nodeType = lastNameId == 0 ? XmlNodeType.EndElement : XmlNodeType.Element;
                depth--; //openTags.Pop();
                //if (nodeType != xr.NodeType) throw new Exception();
                if (nodeType == XmlNodeType.EndElement)
                    return true;
            }

            if (!nameTable.ContainsKey(Math.Abs(lastNameId)))
                nameTable.Add(Math.Abs(lastNameId), br.ReadString());

            nodeType = XmlNodeType.Element;
            //if (nodeType != xr.NodeType || xr.LocalName != nameTable[lastNameId]) throw new Exception();

            depth++;//openTags.Push(nameTable[lastNameId]);

            for (; ; )
            {
                lastNameId = br.ReadInt16();
                //if (lastNameId == 0)
                //{
                //    openTags.Pop();
                //    lastNameId = br.ReadInt16();
                //    break;
                //}
                //else
                if (lastNameId >= 0)
                {
                    isEmptyElement = lastNameId == 0;
                    break;
                }

                if (!nameTable.ContainsKey(Math.Abs(lastNameId)))
                    nameTable.Add(Math.Abs(lastNameId), br.ReadString());
                //Attributes.Add(new KeyValuePair<string, string>(nameTable[lastNameId], br.ReadString()));
                Attributes.Add(nameTable[Math.Abs(lastNameId)], br.ReadString());
            }

            

            return true;
        }

        public override bool ReadAttributeValue()
        {
            throw new NotImplementedException();
        }

        public override ReadState ReadState
        {
            get { throw new NotImplementedException(); }
        }

        public override void ResolveEntity()
        {
            throw new NotImplementedException();
        }

        public override string Value
        {
            get { throw new NotImplementedException(); }
        }
    }

    class BinaryXMLWritter : XmlWriter
    {
        private System.IO.FileStream file;
        System.IO.BinaryWriter bw;

        Dictionary<string, short> nameTable = new Dictionary<string, short>();
        short nextNameId = 1;

        private void WriteNameId(string name, bool isAttribute)
        {
            if (!nameTable.ContainsKey(name))
            {
                bw.Write((short)(isAttribute ? -nextNameId : nextNameId));
                nameTable.Add(name, nextNameId);
                bw.Write(name);
                nextNameId++;
            }
            else
            {
                var id = nameTable[name];
                bw.Write((short)(isAttribute ? -id : id));
            }
        }

        public BinaryXMLWritter(System.IO.FileStream file)
        {
            // TODO: Complete member initialization
            this.file = file;
            bw = new System.IO.BinaryWriter(file, Encoding.ASCII);
        }

        public override void WriteStartElement(string prefix, string localName, string ns)
        {
            WriteNameId(localName, false);
        }

        public override void WriteEndElement()
        {
            bw.Write((short)0);
        }

        public override void WriteStartAttribute(string prefix, string localName, string ns)
        {
            WriteNameId(localName, true);
        }

        public override void WriteEndAttribute()
        {
        }

        public override void WriteString(string text)
        {
            bw.Write(text);
        }

        public override void Flush()
        {
            bw.Flush();
        }

        #region Not implemented

        public override void Close()
        {
            throw new NotImplementedException();
        }

        public override string LookupPrefix(string ns)
        {
            throw new NotImplementedException();
        }

        public override void WriteBase64(byte[] buffer, int index, int count)
        {
            throw new NotImplementedException();
        }

        public override void WriteCData(string text)
        {
            throw new NotImplementedException();
        }

        public override void WriteCharEntity(char ch)
        {
            throw new NotImplementedException();
        }

        public override void WriteChars(char[] buffer, int index, int count)
        {
            throw new NotImplementedException();
        }

        public override void WriteComment(string text)
        {
            throw new NotImplementedException();
        }

        public override void WriteDocType(string name, string pubid, string sysid, string subset)
        {
            throw new NotImplementedException();
        }

        public override void WriteEntityRef(string name)
        {
            throw new NotImplementedException();
        }

        public override void WriteFullEndElement()
        {
            throw new NotImplementedException();
        }

        public override void WriteProcessingInstruction(string name, string text)
        {
            throw new NotImplementedException();
        }

        public override void WriteRaw(string data)
        {
            throw new NotImplementedException();
        }

        public override void WriteRaw(char[] buffer, int index, int count)
        {
            throw new NotImplementedException();
        }

        public override void WriteStartDocument(bool standalone)
        {
            throw new NotImplementedException();
        }

        public override WriteState WriteState
        {
            get { throw new NotImplementedException(); }
        }

        public override void WriteSurrogateCharEntity(char lowChar, char highChar)
        {
            throw new NotImplementedException();
        }

        public override void WriteWhitespace(string ws)
        {
            throw new NotImplementedException();
        }

        public override void WriteStartDocument()
        {

        }

        public override void WriteEndDocument()
        {
            throw new NotImplementedException();
        }

#endregion
    }
}
