﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using AgentCoin.Properties;
using System.Collections;
using System.Diagnostics;

namespace AgentCoin
{
    public partial class MainClass : ApplicationContext
    {
        private System.Windows.Forms.NotifyIcon MainNotifyIcon;
        private System.Windows.Forms.ContextMenuStrip TrayMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ExitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowFolderMenuItem;
        private System.Windows.Forms.Timer CheckTimer;
        private System.Windows.Forms.ToolStripMenuItem CheckNowMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem AutodownloadMenuItem;
        private System.ComponentModel.BackgroundWorker CurrentTaskBackgroundWorker;

        Logger logger = new Logger("log.txt");

        Queue<QueuedTask> tasks = new Queue<QueuedTask>();
        QueuedTask currentTask = null;

        List<QueuedTask> OutOfOrderTasks = new List<QueuedTask>();

        bool isExiting = false;

        string lastDownloadedDBPath = null;
        bool show_lastDownloadedDBPath_onBaloonClick = false;


       private void InitializeComponent()
        {
            this.MainNotifyIcon = new System.Windows.Forms.NotifyIcon();
            this.TrayMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            this.CheckNowMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AutodownloadMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ExitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowFolderMenuItem = new ToolStripMenuItem();

            this.CheckTimer = new System.Windows.Forms.Timer();

            this.CurrentTaskBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            // 
            // MainNotifyIcon
            // 
            this.MainNotifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.MainNotifyIcon.BalloonTipTitle = "Мониторинг CME DB";
            this.MainNotifyIcon.ContextMenuStrip = this.TrayMenuStrip;
            this.MainNotifyIcon.Text = "Agent Coin";
            this.MainNotifyIcon.Visible = true;
            this.MainNotifyIcon.BalloonTipClicked += new System.EventHandler(this.MainNotifyIcon_BalloonTipClicked);
            this.MainNotifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainNotifyIcon_MouseClick);
            // 
            // TrayMenuStrip
            // 
            this.TrayMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CheckNowMenuItem,
            this.ShowFolderMenuItem,
            this.AutodownloadMenuItem,
            this.toolStripSeparator1,
            this.ExitMenuItem});
            this.TrayMenuStrip.Name = "contextMenuStrip1";
            this.TrayMenuStrip.Size = new System.Drawing.Size(218, 120);
            // 
            // CheckNowMenuItem
            // 
            this.CheckNowMenuItem.Name = "CheckNowMenuItem";
            this.CheckNowMenuItem.Size = new System.Drawing.Size(217, 22);
            this.CheckNowMenuItem.Text = "Проверить сейчас";
            this.CheckNowMenuItem.Click += new System.EventHandler(this.CheckNowMenuItem_Click);
            // 
            // ShowFolderMenuItem
            // 
            this.ShowFolderMenuItem.Name = "ShowFolderMenuItem";
            this.ShowFolderMenuItem.Size = new System.Drawing.Size(217, 22);
            this.ShowFolderMenuItem.Text = "Показать папку";
            this.ShowFolderMenuItem.Click += new EventHandler(ShowFolderMenuItem_Click);
            // 
            // AutodownloadMenuItem
            // 
            this.AutodownloadMenuItem.Name = "AutodownloadMenuItem";
            this.AutodownloadMenuItem.Size = new System.Drawing.Size(217, 22);
            this.AutodownloadMenuItem.Text = "Скачивать автоматически";
            this.AutodownloadMenuItem.Click += new System.EventHandler(this.AutodownloadMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Size = new System.Drawing.Size(198, 6);
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Name = "ExitMenuItem";
            this.ExitMenuItem.Size = new System.Drawing.Size(217, 22);
            this.ExitMenuItem.Text = "Выход";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // CheckTimer
            // 
            this.CheckTimer.Interval = 5000;
            this.CheckTimer.Tick += new System.EventHandler(this.CheckTimer_Tick);
            // 
            // CurrentTaskBackgroundWorker
            // 
            this.CurrentTaskBackgroundWorker.WorkerSupportsCancellation = true;
            this.CurrentTaskBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.CurrentTaskBackgroundWorker_DoWork);
            this.CurrentTaskBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.CurrentTaskBackgroundWorker_RunWorkerCompleted);
        }

        public MainClass()
        {
            InitializeComponent();
            MainNotifyIcon.Icon = Resources.AgentNoNews;
            tasks.Enqueue(new CheckNewDBTask(DateTime.Now, this.logger));
            tasks.Enqueue(new CheckCOT(DateTime.Now, this.logger));
            tasks.Enqueue(new CheckForwardPoints(DateTime.Now, this.logger));

            CheckTimer.Start();
            AutodownloadMenuItem.Checked = Settings.Default.Autodownload;
        }

        private void CheckTimer_Tick(object sender, EventArgs e)
        {
            if (currentTask != null)
                return;

            if (isExiting)
            {
                this.MainNotifyIcon.Dispose();
                Application.Exit();
                return;
            }

            currentTask = tasks.Dequeue();
            if (currentTask.StartTime <= DateTime.Now)
                CurrentTaskBackgroundWorker.RunWorkerAsync(currentTask);
            else
            {
                tasks.Enqueue(currentTask);
                currentTask = null;
            }
        }

        private void CurrentTaskBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var task = (QueuedTask)e.Argument;
            task.Execute(()=>CurrentTaskBackgroundWorker.CancellationPending);
        }

        private void CurrentTaskBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Notification n;
            var newTasks = currentTask.OnAfterCompleted(out n);

            if (n != null)
            {
                if (currentTask is DownloadDBTask)
                {
                    lastDownloadedDBPath = (currentTask as DownloadDBTask).downloadedFilePath;
                    show_lastDownloadedDBPath_onBaloonClick = true;
                }
                else
                    show_lastDownloadedDBPath_onBaloonClick = false;

                MainNotifyIcon.Icon = n.AgentIcon;
                MainNotifyIcon.ShowBalloonTip(0, n.TipTitle, n.TipText, n.IsError ? ToolTipIcon.Error : ToolTipIcon.Info);
            }

            currentTask = null;
            foreach (var task in newTasks)
                tasks.Enqueue(task);
        }

        private void AutodownloadMenuItem_Click(object sender, EventArgs e)
        {
            AutodownloadMenuItem.Checked = !AutodownloadMenuItem.Checked;
            Settings.Default.Autodownload = AutodownloadMenuItem.Checked;
            Settings.Default.Save();
        }

        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            this.isExiting = true;
            CurrentTaskBackgroundWorker.CancelAsync();
        }

        private void MainNotifyIcon_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                MainNotifyIcon.Icon = Resources.AgentNoNews;
        }

        private void MainNotifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            MainNotifyIcon.Icon = Resources.AgentNoNews;
            if (show_lastDownloadedDBPath_onBaloonClick)
                ShowDownloads();
        }

        private void CheckNowMenuItem_Click(object sender, EventArgs e)
        {
            List<QueuedTask> otherTasks = new List<QueuedTask>();
            var tasks = this.tasks.ToArray();
            this.tasks.Clear();
            foreach (var task in tasks)
            {
                if (task is CheckCOT || task is CheckNewDBTask)
                {
                    task.StartTime = DateTime.Now;
                    this.tasks.Enqueue(task);
                }
                else
                    otherTasks.Add(task);
            }

            foreach (var task in otherTasks)
                this.tasks.Enqueue(task);
        }

        void ShowFolderMenuItem_Click(object sender, EventArgs e)
        {
            ShowDownloads();
        }

        private void ShowDownloads()
        {
            string p = string.IsNullOrEmpty(lastDownloadedDBPath) ? Program.DbDownloadsPath : lastDownloadedDBPath;
            string args = string.Format("/e, /select, \"{0}\"", p);

            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = "explorer";
            info.Arguments = args;
            Process.Start(info);
        }
    }
}
