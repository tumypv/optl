﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgentCoin
{
    class FtpFile
    {
        static string[] mnths = new string[] { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };

        public string Attributes { get; set; }
        public long Size { get; set; }
        public DateTime ModificationTime { get; set; }
        public bool IsModificationTimePrecise { get; set; }
        public string Name { get; set; }

        public string OriginalText { get; set; }

        public static FtpFile Parse(string text)
        {
            var res = new FtpFile();
            res.OriginalText = text;
            res.Attributes = text.Substring(0, 10).Trim();
            //var f1 = line.Substring(10, 5).Trim();
            //var f2 = line.Substring(15, 6).Trim();
            //var f3 = line.Substring(21, 5).Trim();
/*
-rw-r--r--    1 ftp      ftp       5067806 Jan 03  2014 DailyBulletin_pdf_201401020.zip
*/
            res.Size = long.Parse(text.Substring(30, 13));

            var mt = text.Substring(42, 13).Trim();
            if (mt.Contains(":"))
            {
                //Nov 07 10:14
                res.IsModificationTimePrecise = true;
                int month = Array.IndexOf<string>(mnths, mt.Substring(0, 3).ToUpper()) + 1;
                var day = int.Parse(mt.Substring(4, 2));
                var hh = int.Parse(mt.Substring(7, 2));
                var mm = int.Parse(mt.Substring(10, 2));
                int yy;
                var currentMonth = DateTime.Now.Month;
                //Чтобы быть месяцем предыдущего года, нужно быть сильно больше, чем текущий (на 6 месяцев для linux).
                //+1 добавляем чтобы скомпенсировать разницу в часовых поясах и наверняка избежать ошибки
                //Таким образом если месяц файла больше текущего + 1, он точно месяц предидущего года
                //например если текущий месяц декабрь, нужно быть 14 месяцем, чтобы быть месяцем из предидущего года ;)
                if (month > currentMonth + 1)
                    yy = DateTime.Now.Year - 1;
                else
                    yy = DateTime.Now.Year;
                res.ModificationTime = new DateTime(yy, month, day, hh, mm, 00);
            }
            else
            {
                res.IsModificationTimePrecise = false;
                res.ModificationTime = DateTime.Parse(mt);
            }

            res.Name = text.Substring(55).Trim();
            return res;
        }

        public override bool Equals(object obj)
        {
            var other = (FtpFile)obj;

            if (this.IsModificationTimePrecise && other.IsModificationTimePrecise)
            {
                var dateDiff = Math.Abs(this.ModificationTime.Subtract(other.ModificationTime).TotalMinutes);
                return dateDiff == 0 && this.Name == other.Name && this.Size == other.Size;
            }
            else
            {
                //Не важно какая там дата, если она не точна. Сравниваем только по размеру и имени файла.
                return this.Name == other.Name && this.Size == other.Size;
            }
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        public override string ToString()
        {
            return OriginalText;
        }
    }
}
